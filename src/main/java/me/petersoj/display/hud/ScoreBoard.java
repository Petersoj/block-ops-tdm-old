package me.petersoj.display.hud;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_10_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo;

public class ScoreBoard{
	
	public ScoreBoard(){
		
		//task = Bukkit.getServer().getScheduler().runTaskTimer(BlockOps.getInstance(), this, 10, 7);
	}
	
	public void registerScoreboard(Player p){
		Scoreboard sb = Bukkit.getServer().getScoreboardManager().getNewScoreboard();
		Objective ob = sb.registerNewObjective("BlockOps", "dummy");
		ob.setDisplaySlot(DisplaySlot.SIDEBAR);
		ob.setDisplayName(ChatColor.BOLD + "" + ChatColor.GOLD + "BLOCK OPS");
		
		Team redShow = sb.registerNewTeam("redShow");
		redShow.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
		redShow.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.ALWAYS);
		redShow.setPrefix(ChatColor.RED.toString());
		
		Team redHide = sb.registerNewTeam("redHide");
		redHide.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
		redHide.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.NEVER);
		redHide.setPrefix(ChatColor.RED.toString());
		
		Team blueShow = sb.registerNewTeam("blueShow");
		blueShow.setOption(Option.COLLISION_RULE, OptionStatus.NEVER);
		blueShow.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.ALWAYS);
		blueShow.setAllowFriendlyFire(false);
		blueShow.setPrefix(ChatColor.BLUE.toString());
		
		blueShow.addEntry(p.getName());
		p.setScoreboard(sb);
	}
	
	public void setEnemyNameTagShown(Player p, Player target){
		if(p.getScoreboard().getTeam("redShow").hasEntry(target.getName())){
			return;
		}
		if(p.getScoreboard().getTeam("redHide").hasEntry(target.getName())){
			p.getScoreboard().getTeam("redHide").removeEntry(target.getName());
		}
		p.getScoreboard().getTeam("redShow").addEntry(target.getName());
	}
	
	public void setEnemyNameTagHiden(Player p, Player target){
		if(p.getScoreboard().getTeam("redHide").hasEntry(target.getName())){
			return;
		}
		if(p.getScoreboard().getTeam("redShow").hasEntry(target.getName())){
			p.getScoreboard().getTeam("redShow").removeEntry(target.getName());
		}
		p.getScoreboard().getTeam("redHide").addEntry(target.getName());
	}
	
	public void setSpecificPlayerTab(Player seeing, Player target, String newName){
		EntityPlayer nmsTargetPlayer = ((CraftPlayer)target).getHandle();
		nmsTargetPlayer.listName = CraftChatMessage.fromString(newName)[0];
		((CraftPlayer)seeing).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.UPDATE_DISPLAY_NAME, nmsTargetPlayer));
	}
	
	
	public void setPlayerScoreBoardLine(Player player, String text, int row){
		for (String entry : player.getScoreboard().getEntries()) {
			if (player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore(entry).getScore() == row) {
				player.getScoreboard().resetScores(entry);
				break;
			}
		}
		player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).getScore(text).setScore(row);
	}
	
	
	
	/*// for animated text Later
	 * 
	public void run(){
		index++;
		if(index <= scoreboard.getObjective(DisplaySlot.SIDEBAR).getDisplayName().length()){
			String oldText = scoreboard.getObjective(DisplaySlot.SIDEBAR).getDisplayName();
			String setText = ChatColor.BOLD +"" + ChatColor.DARK_GRAY + oldText.substring(0, index) + ChatColor.BOLD + ChatColor.GOLD 
					+ oldText.charAt(index) + ChatColor.BOLD + ChatColor.DARK_GRAY + oldText.substring(index+1); 
			scoreboard.getObjective(DisplaySlot.SIDEBAR).setDisplayName(setText);
		}else if(index <= 40){
			
		}else{
			
		}
	}
	 */
	
	
	
}
