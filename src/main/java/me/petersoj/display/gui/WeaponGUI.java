package me.petersoj.display.gui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;
import me.petersoj.weapons.grenades.Lethal;
import me.petersoj.weapons.grenades.Tactical;
import me.petersoj.weapons.guns.Attachment;
import me.petersoj.weapons.guns.Gun;
import me.petersoj.weapons.guns.GunClass;
import me.petersoj.weapons.guns.GunManager.GunInfo;
import me.petersoj.weapons.perk.Perk;

public class WeaponGUI implements Listener{
	
	public ItemStack smg,assault,shotgun,sniper,pistol,launcher,special;
	public ItemStack topAndBottomLine, leftAndRightLine, bottomLine, topLine, leftLine, rightLine;
	public ItemStack backButton, noneItem;
	public ItemStack createAClass, lockedClass2, lockedClass3, lockedClass4, scoreStreak1;
	public ItemStack cantPrestige, canPrestige;
	
	public WeaponGUI(){
		Bukkit.getServer().getPluginManager().registerEvents(this, BlockOps.getInstance());
		smg = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.PUMA.durability);
		assault = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.L4R.durability);
		shotgun = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.T1M2.durability);
		sniper = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.SHONE.durability);
		pistol = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.PM3.durability);
		launcher = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.SMEGA.durability);
		special = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)GunInfo.COMBAT_KNIFE.durability);
		this.giveName(smg, ChatColor.WHITE + "Submachine Guns", null);
		this.giveName(assault, ChatColor.WHITE + "Assault Rifles", null);
		this.giveName(shotgun, ChatColor.WHITE + "Shotguns", null);
		this.giveName(sniper, ChatColor.WHITE + "Snipers", null);
		this.giveName(pistol, ChatColor.WHITE + "Pistols", null);
		this.giveName(launcher, ChatColor.WHITE + "Launchers", null);
		this.giveName(special, ChatColor.WHITE + "Special", null);
		
		topAndBottomLine = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)3); //TODO GUI durability
		leftAndRightLine= new ItemStack(Material.DIAMOND_PICKAXE,1,(short)3);
		bottomLine = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)3);
		topLine = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)3);
		leftLine = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)5);
		rightLine = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)5);
		backButton = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		noneItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		createAClass = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		scoreStreak1 = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		lockedClass2 = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		lockedClass3 = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		lockedClass4 = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)6);
		for(ItemStack item : new ItemStack[]{topAndBottomLine,leftAndRightLine,bottomLine,topLine,leftLine,rightLine}){
			this.giveName(item, ChatColor.WHITE + "", null);
		}
		this.giveName(noneItem, ChatColor.BLUE + "None", null);
		this.giveName(backButton, ChatColor.BLUE + "Back", null);
		this.giveName(createAClass, ChatColor.GOLD + "" + ChatColor.BOLD + "Create A Class", null);
		this.giveName(scoreStreak1, ChatColor.BLUE + "" + ChatColor.BOLD + "Set ScoreStreak", null);
		this.giveName(backButton, ChatColor.BLUE + "Back", null);
		this.giveName(lockedClass2, ChatColor.GOLD + "" + ChatColor.BOLD + "Class 2" + ChatColor.RED + ChatColor.BOLD + " - LOCKED", Arrays.asList(ChatColor.GRAY + "Unlock in the Store"));
		this.giveName(lockedClass3, ChatColor.GOLD + "" + ChatColor.BOLD + "Class 3" + ChatColor.RED + ChatColor.BOLD + " - LOCKED", Arrays.asList(ChatColor.GRAY + "Unlock in the Store"));
		this.giveName(lockedClass4, ChatColor.GOLD + "" + ChatColor.BOLD + "Class 4" + ChatColor.RED + ChatColor.BOLD + " - LOCKED", Arrays.asList(ChatColor.GRAY + "Unlock in the Store"));
	}
	
	public void giveName(ItemStack item, String name, List<String> list){
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(name);
		im.setLore(list);
		im.spigot().setUnbreakable(true);
		im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(im);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){  // Gun Inventory CLick Event
		Player p = (Player)e.getWhoClicked();
		PlayerData pData = BlockOps.getDataManager().getPlayerData(p);
		Inventory inv = e.getInventory();
		ItemStack item = e.getCurrentItem();
		e.setCancelled(true);
		if(item == null){ return; }
		if(item.equals(bottomLine) || item.equals(topLine) || item.equals(leftLine) || item.equals(rightLine) || item.equals(topAndBottomLine) || item.equals(leftAndRightLine)){
			return;
		}else if(item.equals(backButton)){
			this.loadClassInventory(pData, pData.selectedClass);
			p.openInventory(pData.inv6);
			return;
		}
		if(inv.equals(pData.inv4)){
			if(item.equals(this.createAClass)){
				pData.selectedClass = pData.class1;
				this.loadClassInventory(pData, pData.class1);
				p.openInventory(pData.inv6);
				return;
			}else if(item.equals(this.scoreStreak1)){
				p.closeInventory();
				p.sendMessage(BlockOps.getMiscMethods().getFormedString("Menu", "The scorestreaks are coming soon!"));
				return;
			}else if(item.equals(pData.GUILevelBar)){
				return;
			}
			if(item.equals(this.smg) || item.equals(this.assault) || item.equals(this.shotgun) || item.equals(this.sniper)){
				this.loadPrimaryGunSelectionInventory(pData, item);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}else if(item.equals(this.pistol) || item.equals(this.launcher) || item.equals(this.special)){
				this.loadSecondaryGunSelectionInventory(pData, item);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}
			// guns
			for(Gun gun : BlockOps.getWeaponManager().getGunManager().getDefaultGunArray()){ // locked guns
				if(item.equals(gun.itemStack)){
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Cannot purchase " + ChatColor.WHITE + gun.gunInfo.name + "!"));
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You need to be level " + ChatColor.GOLD + gun.gunInfo.unlockLevel + "."));
					return;
				}
			}
			for(Gun gunLoop : pData.boughtGuns){ // bought guns
				if(item.equals(gunLoop.itemStack)){
					Gun newGun = gunLoop.clone();
					newGun.attachment1 = BlockOps.getWeaponManager().getAttachmentManager().none;
					newGun.attachment2 = BlockOps.getWeaponManager().getAttachmentManager().none;
					newGun.Paccuracy=0;newGun.Pdamage=0;newGun.PfireRate=0;newGun.Prange=0;
					BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, newGun, true, true, false);
					if(pData.guiByte == 0){
						pData.selectedClass.primary = newGun;
					}else if(pData.guiByte == 1){
						pData.selectedClass.secondary = newGun;
					}
					this.loadClassInventory(pData, pData.selectedClass);
					p.openInventory(pData.inv6);
					return;
				}
			}
			ArrayList<Gun> toDeleteGun = null;
			for(Gun gunLoop : pData.unboughtGuns){ // unbought guns
				if(item.equals(gunLoop.itemStack)){
					if(pData.coins >= 2){ // bought the gun
						Gun newSelectedGun = gunLoop.clone();
						newSelectedGun.attachment1 = BlockOps.getWeaponManager().getAttachmentManager().none;
						newSelectedGun.attachment2 = BlockOps.getWeaponManager().getAttachmentManager().none;
						newSelectedGun.Paccuracy=0;newSelectedGun.Pdamage=0;newSelectedGun.PfireRate=0;newSelectedGun.Prange=0;
						BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, newSelectedGun, true, true, false);
						if(pData.guiByte == 0){
							pData.selectedClass.primary = newSelectedGun;
						}else if(pData.guiByte == 1){
							pData.selectedClass.secondary = newSelectedGun;
						}
						toDeleteGun = new ArrayList<Gun>();
						toDeleteGun.add(gunLoop);
						Gun newBoughtGun = gunLoop.clone();
						newBoughtGun.attachment1 = BlockOps.getWeaponManager().getAttachmentManager().none;
						newBoughtGun.attachment2 = BlockOps.getWeaponManager().getAttachmentManager().none;
						BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, newBoughtGun, false, true, false);
						pData.boughtGuns.add(newBoughtGun);
						pData.coins = pData.coins - 2;
						this.loadClassInventory(pData, pData.class1);
						p.updateInventory();
						p.openInventory(pData.inv6);
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.GREEN + "Successfully bought " + ChatColor.WHITE + gunLoop.gunInfo.name));
					}else{ // not enough coins
						p.closeInventory();
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You don't have enought coins!"));
						return;
					}
				}
			}
			if(toDeleteGun != null){ // remove from unbought guns
				for(Gun toDel : toDeleteGun){ 
					pData.unboughtGuns.remove(toDel);
				}
				return;
			}
			if(item.equals(pData.selectedClass.primary.itemStack)){ // already selected guns (enchanted)
				pData.selectedClass.primary.attachment1 = BlockOps.getWeaponManager().getAttachmentManager().none;
				pData.selectedClass.primary.attachment2 = BlockOps.getWeaponManager().getAttachmentManager().none;
				pData.selectedClass.primary.Paccuracy=0;pData.selectedClass.primary.Pdamage=0;
				pData.selectedClass.primary.PfireRate=0;pData.selectedClass.primary.Prange=0;
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(item.equals(pData.selectedClass.secondary.itemStack)){
				pData.selectedClass.secondary.attachment1 = BlockOps.getWeaponManager().getAttachmentManager().none;
				pData.selectedClass.secondary.attachment2 = BlockOps.getWeaponManager().getAttachmentManager().none;
				pData.selectedClass.secondary.Paccuracy=0;pData.selectedClass.secondary.Pdamage=0;
				pData.selectedClass.secondary.PfireRate=0;pData.selectedClass.secondary.Prange=0;
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}
			// attachments
			for(Attachment attach : BlockOps.getWeaponManager().getAttachmentManager().getDefaultAttachmentArray()){ // locked attachments
				if(item.equals(attach.itemstack)){
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Cannot purchase " +  ChatColor.WHITE + attach.attachmentInfo.name + "!"));
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You need to have a weapon level of at least " + ChatColor.GOLD + attach.attachmentInfo.weaponUnlockLevel + "."));
					return;
				}
			}
			for(Attachment attach : BlockOps.getWeaponManager().getAttachmentManager().getUnlockedAttachmentArray()){ // unlocked attachments
				if(item.equals(attach.itemstack)){
					Attachment at = new Attachment(attach.attachmentInfo);
					BlockOps.getWeaponManager().getAttachmentManager().setAttachmentItemStack(pData, at, false, false);
					if(pData.guiByte == 0){
						pData.selectedClass.primary.attachment1 = at;
					}else if(pData.guiByte == 1){
						pData.selectedClass.primary.attachment2 = at;
					}else if(pData.guiByte == 2){
						pData.selectedClass.secondary.attachment1 = at;
					}else if(pData.guiByte == 3){
						pData.selectedClass.secondary.attachment2 = at;
					}
					this.loadClassInventory(pData, pData.selectedClass);
					p.updateInventory();
					p.openInventory(pData.inv6);
					return;
				}
			}
			if(item.equals(pData.selectedClass.primary.attachment1.itemstack) || item.equals(pData.selectedClass.primary.attachment2.itemstack)){   // already selected attachments
				p.openInventory(pData.inv6);
				return;
			}else if(item.equals(pData.selectedClass.secondary.attachment1.itemstack) || item.equals(pData.selectedClass.secondary.attachment2.itemstack)){
				p.openInventory(pData.inv6);
				return;
			}
			// Lethal
			for(Lethal leth : BlockOps.getWeaponManager().getLethalManager().getDefaultLethalArray()){  // locked lethals
				if(item.equals(leth.itemstack)){
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Cannot purchase " +  ChatColor.WHITE + leth.lethalInfo.name + "!"));
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You need to be level " + ChatColor.GOLD + leth.lethalInfo.unlockLevel + "."));
					return;
				}
			}
			for(Lethal leth : pData.boughtLethals){ // bought lethals
				if(item.equals(leth.itemstack)){
					Lethal newLeth = new Lethal(leth.lethalInfo);
					BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, newLeth, true, true, false);
					if(pData.guiByte == 0){
						pData.selectedClass.lethal1 = newLeth;
					}else if(pData.guiByte == 1){
						pData.selectedClass.lethal2 = newLeth;
					}
					return;
				}
			}
			ArrayList<Lethal> toDeleteLethal = null;
			for(Lethal lethalLoop : pData.unboughtLethals){ // unbought lethals
				if(item.equals(lethalLoop.itemstack)){
					if(pData.coins >= 1){ // bought the lethal
						Lethal newSelectedLethal = new Lethal(lethalLoop.lethalInfo);
						BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, newSelectedLethal, true, true, false);
						if(pData.guiByte == 0){
							pData.selectedClass.lethal1 = newSelectedLethal;
						}else if(pData.guiByte == 1){
							pData.selectedClass.lethal2 = newSelectedLethal;
						}
						toDeleteLethal = new ArrayList<Lethal>();
						toDeleteLethal.add(lethalLoop);
						Lethal newBoughtLethal = new Lethal(lethalLoop.lethalInfo);
						BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, newBoughtLethal, false, true, false);
						pData.boughtLethals.add(newBoughtLethal);
						pData.coins = pData.coins - 1;
						this.loadClassInventory(pData, pData.selectedClass);
						p.updateInventory();
						p.openInventory(pData.inv6);
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.GREEN + "Successfully bought " + ChatColor.WHITE + lethalLoop.lethalInfo.name));
					}else{ // not enough coins
						p.closeInventory();
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You don't have enought coins!"));
						return;
					}
				}
			}
			if(toDeleteLethal != null){ // remove from unbought lethals
				for(Lethal leth : toDeleteLethal){ 
					pData.unboughtLethals.remove(leth);
				}
				return;
			}
			if(item.equals(pData.selectedClass.lethal1.itemstack)){ // already selected lethals
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(item.equals(pData.selectedClass.lethal2.itemstack)){
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}
			
			// Tacticals
			for(Tactical tac : BlockOps.getWeaponManager().getTacticalManager().getDefaultTacticalArray()){  // locked tacticals
				if(item.equals(tac.itemstack)){
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Cannot purchase " +  ChatColor.WHITE + tac.tacticalInfo.name + "!"));
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You need to be level " + ChatColor.GOLD + tac.tacticalInfo.unlockLevel + "."));
					return;
				}
			}
			for(Tactical tac : pData.boughtTacticals){ // bought tacticals
				if(item.equals(tac.itemstack)){
					Tactical newTac = new Tactical(tac.tacticalInfo);
					BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, newTac, true, true, false);
					if(pData.guiByte == 0){
						pData.selectedClass.tactical1 = newTac;
					}else if(pData.guiByte == 1){
						pData.selectedClass.tactical2 = newTac;
					}
					return;
				}
			}
			ArrayList<Tactical> toDeleteTactical = null;
			for(Tactical tacticalLoop : pData.unboughtTacticals){ // unbought tacticals
				if(item.equals(tacticalLoop.itemstack)){
					if(pData.coins >= 1){ // bought the tactical
						Tactical newSelectedTactical = new Tactical(tacticalLoop.tacticalInfo);
						BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, newSelectedTactical, true, true, false);
						if(pData.guiByte == 0){
							pData.selectedClass.tactical1 = newSelectedTactical;
						}else if(pData.guiByte == 1){
							pData.selectedClass.tactical2 = newSelectedTactical;
						}
						toDeleteTactical = new ArrayList<Tactical>();
						toDeleteTactical.add(tacticalLoop);
						Tactical newBoughtTactical = new Tactical(tacticalLoop.tacticalInfo);
						BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, newBoughtTactical, false, true, false);
						pData.boughtTacticals.add(newBoughtTactical);
						pData.coins = pData.coins - 1;
						this.loadClassInventory(pData, pData.selectedClass);
						p.updateInventory();
						p.openInventory(pData.inv6);
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.GREEN + "Successfully bought " + ChatColor.WHITE + tacticalLoop.tacticalInfo.name));
					}else{ // not enough coins
						p.closeInventory();
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You don't have enought coins!"));
						return;
					}
				}
			}
			if(toDeleteTactical != null){ // remove from unbought tacticals
				for(Tactical tac : toDeleteTactical){ 
					pData.unboughtTacticals.remove(tac);
				}
				return;
			}
			if(item.equals(pData.selectedClass.tactical1.itemstack)){ // already selected tacticals
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(item.equals(pData.selectedClass.tactical2.itemstack)){
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}
			
			// Perks
			for(Perk perk : BlockOps.getWeaponManager().getPerkManager().getDefaultPerkArray()){  // locked perks
				if(item.equals(perk.itemstack)){
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Cannot purchase " +  ChatColor.WHITE + perk.perkInfo.name + "!"));
					p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You need to be level " + ChatColor.GOLD + perk.perkInfo.unlockLevel + "."));
					return;
				}
			}
			for(Perk perk : pData.boughtPerks){ // bought Perks
				if(item.equals(perk.itemstack)){
					Perk newPerk = new Perk(perk.perkInfo);
					BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, newPerk, true, true, false);
					pData.selectedClass.perk = newPerk;
					return;
				}
			}
			ArrayList<Perk> toDeletePerk = null;
			for(Perk perkLoop : pData.boughtPerks){ // unbought Perks
				if(item.equals(perkLoop.itemstack)){
					if(pData.coins >= 1){ // bought the perks
						Perk newSelectedPerk = new Perk(perkLoop.perkInfo);
						BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, newSelectedPerk, true, true, false);
						pData.selectedClass.perk = newSelectedPerk;
						toDeletePerk = new ArrayList<Perk>();
						toDeletePerk.add(perkLoop);
						Perk newBoughtPerk = new Perk(perkLoop.perkInfo);
						BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, newBoughtPerk, false, true, false);
						pData.boughtPerks.add(newBoughtPerk);
						pData.coins = pData.coins - 1;
						this.loadClassInventory(pData, pData.selectedClass);
						p.updateInventory();
						p.openInventory(pData.inv6);
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.GREEN + "Successfully bought " + ChatColor.WHITE + perkLoop.perkInfo.name));
					}else{ // not enough coins
						p.closeInventory();
						p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "You don't have enought coins!"));
						return;
					}
				}
			}
			if(toDeletePerk != null){ // remove from unbought perks
				for(Perk perk : toDeletePerk){ 
					pData.unboughtPerks.remove(perk);
				}
				return;
			}
			if(item.equals(pData.selectedClass.perk.itemstack)){ // already selected perk
				this.loadClassInventory(pData, pData.selectedClass);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}
		}else if(inv.equals(pData.inv6)){
			if(item.equals(pData.class1.classItemStack)){
				pData.selectedClass = pData.class1;
				this.loadClassInventory(pData, pData.class1);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(pData.class2 != null && item.equals(pData.class2.classItemStack)){
				pData.selectedClass = pData.class2;
				this.loadClassInventory(pData, pData.class2);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(pData.class3 != null && item.equals(pData.class3.classItemStack)){
				pData.selectedClass = pData.class3;
				this.loadClassInventory(pData, pData.class3);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}else if(pData.class4 != null && item.equals(pData.class4.classItemStack)){
				pData.selectedClass = pData.class4;
				this.loadClassInventory(pData, pData.class4);
				p.updateInventory();
				p.openInventory(pData.inv6);
				return;
			}
			if(item.equals(lockedClass2) || item.equals(lockedClass3) || item.equals(lockedClass4)){
				p.closeInventory();
				p.sendMessage(BlockOps.getMiscMethods().getFormedString("Class", ChatColor.RED + "Purchase this class at " + ChatColor.GREEN + "block-ops.com"));
				return;
			}
			if(item.equals(pData.selectedClass.primary.itemStack)){
				pData.guiByte = 0;
				this.loadPrimaryGunSelectionInventory(pData, smg);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}else if(item.equals(pData.selectedClass.secondary.itemStack)){
				pData.guiByte = 1;
				this.loadSecondaryGunSelectionInventory(pData, pistol);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}
			if(item.equals(pData.selectedClass.primary.attachment1.itemstack)){
				pData.guiByte = 0;
				this.loadAttachmentSelectionInventory(pData);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}else if(item.equals(pData.selectedClass.primary.attachment2.itemstack)){
				pData.guiByte = 1;
				this.loadAttachmentSelectionInventory(pData);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}else if(item.equals(pData.selectedClass.secondary.attachment1.itemstack)){
				pData.guiByte = 2;
				this.loadAttachmentSelectionInventory(pData);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}else if(item.equals(pData.selectedClass.secondary.attachment2.itemstack)){
				pData.guiByte = 3;
				this.loadAttachmentSelectionInventory(pData);
				p.updateInventory();
				p.openInventory(pData.inv4);
				return;
			}
			if(item.equals(pData.selectedClass.lethal1.itemstack)){
				return;
			}else if(item.equals(pData.selectedClass.lethal2.itemstack)){
				return;
			}
			if(item.equals(pData.selectedClass.tactical1.itemstack)){
				return;
			}else if(item.equals(pData.selectedClass.tactical2.itemstack)){
				return;
			}
			
			if(item.equals(pData.selectedClass.perk.itemstack)){
				return;
			}
		}
	}
	
	@EventHandler
	public void invCloseEvent(InventoryCloseEvent e){
		Inventory inv = e.getInventory();
		PlayerData pData = BlockOps.getDataManager().getPlayerData((Player)e.getPlayer());
		if(pData != null && (inv.equals(pData.inv4) || inv.equals(pData.inv6))){
			Bukkit.broadcastMessage("1");
			for(ItemStack item : inv.getContents()){
				if(item != null){
					if(item.containsEnchantment(Enchantment.DIG_SPEED)){
						Bukkit.broadcastMessage("removed");
						item.removeEnchantment(Enchantment.DIG_SPEED);
					}
				}
			}
		}
	}
	
	
	public void loadPrimaryGunSelectionInventory(PlayerData pData, ItemStack toLoad){
		GunClass selectedClass = pData.selectedClass;
		Inventory inv = pData.inv4;
		inv.clear();
		toLoad.addEnchantment(Enchantment.DIG_SPEED, 1);
		Gun[] gunArray = null;
		if(toLoad.equals(smg)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getSMGGunArray();
		}else if(toLoad.equals(assault)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getAssaultGunArray();
		}else if(toLoad.equals(shotgun)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getShotgunGunArray();
		}else if(toLoad.equals(sniper)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getSniperGunArray();
		}
		inv.setItem(1, smg);
		inv.setItem(3, assault);
		inv.setItem(5, shotgun);
		inv.setItem(7, sniper);
		inv.setItem(27, backButton);
		int itemSlot = 18;
		for(Gun lockedGun : gunArray){
			ItemStack toSet = null;
			itemSlot++;
			if(lockedGun.gunInfo == selectedClass.primary.gunInfo){
				selectedClass.primary.itemStack.addEnchantment(Enchantment.DIG_SPEED, 1);
				toSet = selectedClass.primary.itemStack;
				Bukkit.broadcastMessage("1");
			}
			if(toSet != null){
				for(Gun unboughtGun : pData.unboughtGuns){
					if(lockedGun.gunInfo == unboughtGun.gunInfo){
						Bukkit.broadcastMessage("2");
						toSet = unboughtGun.itemStack;
						break;
					}
				}
			}
			if(toSet != null){
				for(Gun boughtGun : pData.boughtGuns){
					if(lockedGun.gunInfo == boughtGun.gunInfo){
						Bukkit.broadcastMessage("3");
						toSet = boughtGun.itemStack;
						break;
					}
				}
			}
			if(toSet == null){
				Bukkit.broadcastMessage("5");
				toSet = lockedGun.itemStack;
			}
			inv.setItem(itemSlot, toSet);
			if((itemSlot%2)==0){ // even
				inv.setItem(itemSlot-9, this.topLine);
			}else{ // odd
				inv.setItem(itemSlot-9, this.topAndBottomLine);
			}
			inv.setItem(itemSlot+9, this.topLine);
		}
		inv.setItem(18, this.rightLine);
		inv.setItem(itemSlot+1, this.leftLine);
	}
	
	public void loadSecondaryGunSelectionInventory(PlayerData pData, ItemStack toLoad){
		GunClass selectedClass = pData.selectedClass;
		Inventory inv = pData.inv4;
		inv.clear();
		toLoad.addEnchantment(Enchantment.DIG_SPEED, 1);
		Gun[] gunArray = null;
		if(toLoad.equals(pistol)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getPistolGunArray();
		}else if(toLoad.equals(launcher)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getLauncherGunArray();
		}else if(toLoad.equals(special)){
			gunArray = BlockOps.getWeaponManager().getGunManager().getSpecialGunArray();
		}
		inv.setItem(2, pistol);
		inv.setItem(4, launcher);
		inv.setItem(6, special);
		inv.setItem(27, backButton);
		int itemSlot = 18;
		for(Gun lockedGun : gunArray){
			ItemStack toSet = null;
			itemSlot++;
			if(lockedGun.gunInfo == selectedClass.secondary.gunInfo){
				selectedClass.secondary.itemStack.addEnchantment(Enchantment.DIG_SPEED, 1);
				toSet = selectedClass.secondary.itemStack;
			}
			for(Gun unboughtGun : pData.unboughtGuns){
				if(lockedGun.gunInfo == unboughtGun.gunInfo && toSet != null){
					toSet = unboughtGun.itemStack;
					break;
				}
			}
			for(Gun boughtGun : pData.boughtGuns){
				if(lockedGun.gunInfo == boughtGun.gunInfo && toSet != null){
					toSet = boughtGun.itemStack;
					break;
				}
			}
			if(toSet == null){
				toSet = lockedGun.itemStack;
			}
			inv.setItem(itemSlot, toSet);
			if((itemSlot%2)==0){ // even
				inv.setItem(itemSlot-9, this.topLine);
			}else{ // odd
				inv.setItem(itemSlot-9, this.topAndBottomLine);
			}
			inv.setItem(itemSlot+9, this.topLine);
		}
		inv.setItem(itemSlot+1, this.leftLine);
	}
	
	public void loadAttachmentSelectionInventory(PlayerData pData){
		GunClass selectedClass = pData.selectedClass;
		Inventory inv = pData.inv4;
		inv.clear();
		inv.setItem(27, backButton);
		for(int slot = 1; slot <= 7; slot++){
			inv.setItem(slot, bottomLine);
		}
		inv.setItem(9, rightLine);
		inv.setItem(18, rightLine);
		inv.setItem(17, leftLine);
		for(int slotBelow = 19; slotBelow<= 5; slotBelow++){
			if(slotBelow <= 22){
				inv.setItem(slotBelow+9, topLine);
			}else{
				inv.setItem(slotBelow, topLine);
			}
		}
		
		
	}
	
	
	public void loadClassInventory(PlayerData pData, GunClass gClass){
		Inventory inv = pData.inv6;
		inv.clear();
		if(gClass == null){
			return;
		}
		for(int slot = 1; slot<=7; slot=slot+2){
			ItemStack itemToSet;
			if(slot == 1){
				itemToSet = pData.class1.classItemStack;
			}else if(slot == 3){
				itemToSet = ((pData.class2 == null) ? this.lockedClass2 : pData.class2.classItemStack);
			}else if(slot == 5){
				itemToSet = ((pData.class3 == null) ? this.lockedClass3 : pData.class3.classItemStack);
			}else{
				itemToSet = ((pData.class4 == null) ? this.lockedClass4 : pData.class4.classItemStack);
			}
			inv.setItem(slot, itemToSet);
		}
		gClass.classItemStack.addEnchantment(Enchantment.DIG_SPEED, 1);
		inv.setItem(0, rightLine);
		inv.setItem(2, leftAndRightLine);
		inv.setItem(4, leftAndRightLine);
		inv.setItem(6, leftAndRightLine);
		inv.setItem(8, leftLine);
		inv.setItem(10, topAndBottomLine);
		inv.setItem(11, bottomLine);
		inv.setItem(12, topAndBottomLine);
		inv.setItem(14, topAndBottomLine);
		inv.setItem(15, bottomLine);
		inv.setItem(16, topAndBottomLine);
		inv.setItem(18, rightLine);
		inv.setItem(19, gClass.primary.itemStack);
		inv.setItem(20, gClass.primary.attachment1.itemstack);
		inv.setItem(21, gClass.primary.attachment2.itemstack);
		inv.setItem(22, leftAndRightLine);
		inv.setItem(23, gClass.secondary.itemStack);
		inv.setItem(24, gClass.secondary.attachment1.itemstack);
		inv.setItem(25, gClass.secondary.attachment2.itemstack);
		inv.setItem(26, leftLine);
		inv.setItem(28, topAndBottomLine);
		inv.setItem(29, topLine);
		inv.setItem(30, topAndBottomLine);
		inv.setItem(32, topAndBottomLine);
		inv.setItem(33, topLine);
		inv.setItem(34, topLine);
		inv.setItem(36, rightLine);
		inv.setItem(37, gClass.lethal1.itemstack);
		inv.setItem(38, leftAndRightLine);
		inv.setItem(39, gClass.tactical1.itemstack);
		inv.setItem(40, leftAndRightLine);
		inv.setItem(41, gClass.perk.itemstack);
		inv.setItem(42, leftLine);
		inv.setItem(45, rightLine);
		inv.setItem(46, gClass.lethal2.itemstack);
		inv.setItem(47, leftAndRightLine);
		inv.setItem(48, gClass.tactical2.itemstack);
		inv.setItem(49, leftLine);
		inv.setItem(50, topLine);
	}
	
	public void loadMainMenu(PlayerData pData){
		Inventory inv = pData.inv4;
		inv.clear();
		inv.setItem(11, createAClass);
		inv.setItem(15, scoreStreak1);
	//	int goTo = 
		for(int a = 27; a<=35; a++){
			inv.setItem(a, pData.GUILevelBar);
		}
	}
	
	
	
	public byte getNumberOfClassSlots(GunClass gClass){
		byte num = 2;
		if(gClass.primary.attachment1 != null) num++;
		if(gClass.primary.attachment2 != null) num++;
		if(gClass.secondary.attachment1 != null) num++;
		if(gClass.secondary.attachment2 != null) num++;
		if(gClass.lethal1 != null) num++;
		if(gClass.lethal2 != null) num++;
		if(gClass.tactical1 != null) num++;
		if(gClass.tactical2 != null) num++;
		if(gClass.perk != null) num++;
		return num;
	}
	
	public void setGUILevelItemStack(PlayerData pData){ //set xp gui level itemstack
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short) 4); // TODO Class item durability value here
		double rand = (1.183215957*pData.level);
		double rand2 = (1.183215957*pData.level+1);
		int xpNeed = (int)((rand2*rand2-1.4)-(rand*rand-1.4));
		this.giveName(item, ChatColor.WHITE + "" + ChatColor.BOLD + "Next Level: " + ChatColor.GOLD + xpNeed + " XP needed", null);
	}
	
	public void setClassItemStacks(PlayerData pData){
		byte index = 0;
		for(GunClass gClass : new GunClass[]{pData.class1, pData.class2, pData.class3, pData.class4}){
			String name = "";
			ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short) 4); // TODO Class item durability value here
			index++;
			if(index == 1){
				name = "Class 1";
			}else if(index == 2){
				name = "Class 2";
			}else if(index == 3){
				name = "Class 3";
			}else if(index == 4){
				name = "Class 4";
			}
			if(gClass != null){
				ItemMeta im = item.getItemMeta();
				StringBuilder sb = new StringBuilder(ChatColor.WHITE + "");
				byte len = this.getNumberOfClassSlots(gClass);
				for(int a = 1; a <= 10; a++){
					if(len+1 == a){
						sb.append(ChatColor.DARK_GRAY + "");
					}else{
						sb.append("█");
					}
				}
				im.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + name + " " + sb.toString() + " " + len + "/10");
				List<String> lore = new ArrayList<String>();
				lore.add("");
				lore.add(ChatColor.GRAY + "Primary: " + gClass.primary.gunInfo.name + " - " + gClass.primary.attachment1.attachmentInfo.name + ", " + gClass.primary.attachment2.attachmentInfo.name);
				lore.add(ChatColor.GRAY + "Secondary: " + gClass.secondary.gunInfo.name + " - " + gClass.secondary.attachment1.attachmentInfo.name + ", " + gClass.secondary.attachment2.attachmentInfo.name);
				lore.add("");
				lore.add(ChatColor.GRAY + "Lethals: " + gClass.lethal1.lethalInfo.name + ", " + gClass.lethal2.lethalInfo.name);
				lore.add(ChatColor.GRAY + "Tacticals: " + gClass.tactical1.tacticalInfo.name + ", " + gClass.tactical2.tacticalInfo.name);
				lore.add("");
				lore.add(ChatColor.GRAY + "Perk: " + gClass.perk.perkInfo.name);
				im.setLore(lore);
				im.spigot().setUnbreakable(true);
				im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
				item.setItemMeta(im);
				gClass.classItemStack = item;
			}
		}
	}
}
