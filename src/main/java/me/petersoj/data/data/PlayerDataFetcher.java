package me.petersoj.data.data;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import me.petersoj.game.BlockOps;
import me.petersoj.misc.Enums.Rank;
import me.petersoj.weapons.grenades.Lethal;
import me.petersoj.weapons.grenades.LethalManager.LethalInfo;
import me.petersoj.weapons.grenades.Tactical;
import me.petersoj.weapons.grenades.TacticalManager.TacticalInfo;
import me.petersoj.weapons.guns.Attachment;
import me.petersoj.weapons.guns.Gun;
import me.petersoj.weapons.guns.GunClass;
import me.petersoj.weapons.guns.GunManager.GunInfo;
import me.petersoj.weapons.perk.Perk;
import me.petersoj.weapons.perk.PerkManager.PerkInfo;
import me.petersoj.weapons.scorestreak.ScoreStreak;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class PlayerDataFetcher implements Runnable{
	
	String uuid;
	Player p;
	Jedis jedis;
	byte tries;
	BukkitTask task;
	PlayerData pData;
	
	public PlayerDataFetcher(Player p){
		this.p = p;
		this.uuid = p.getUniqueId().toString();
		this.tries = 0;
		pData = new PlayerData();
		pData.inv6 = Bukkit.createInventory(p, 54, ChatColor.GOLD + "Gun Classes");
		pData.inv4 = Bukkit.createInventory(p, 36, ChatColor.GOLD + "Menu");
	}
	
	public void executeMethods(){
		task = Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(BlockOps.getInstance(), this, 2, 2);
	}
	
	
	public void run(){
		if(tries == 0){
			try{
				this.jedis = BlockOps.getDataManager().getJedisObj();
			}catch(JedisException e){
				task.cancel();
				return;
			}
		}
		String[] data = null;
		try{
			data = jedis.get(uuid).split("\\|");
			task.cancel();
			jedis.close();
		}catch(JedisException ex){
			tries++;
		}
		if(data != null){
			long diff = System.currentTimeMillis() - Long.valueOf(data[0]);
			if(diff <= 1000){
				tries++;
			}else{
				pData.rank = Rank.valueOf(data[1]);
				pData.xp = Integer.valueOf(data[2]);
				pData.level = BlockOps.getDataManager().getPlayerLevel(pData.xp);
				pData.coins = Integer.valueOf(data[3]);
				String[] bought = data[4].split("-"); // bought data
				ArrayList<GunInfo> boughtGunInfo = new ArrayList<GunInfo>();
				ArrayList<LethalInfo> boughtLethalInfo = new ArrayList<LethalInfo>();
				ArrayList<TacticalInfo> boughtTacticalInfo = new ArrayList<TacticalInfo>();
				ArrayList<PerkInfo> boughtPerkInfo = new ArrayList<PerkInfo>();
				for(String str : bought){  // FOR BOUGHT ITEMS
					if(str.contains("[")){
						String[] split = str.split("\\[");
						Gun g = null;
						char gunChar = split[0].charAt(0);
						for(Gun gunLoop : BlockOps.getWeaponManager().getGunManager().getDefaultGunArray()){
							if(gunLoop.gunInfo.character == gunChar){
								g = gunLoop.clone();
								g.gunKills = Integer.valueOf(split[1]);
								BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, g, false, true, false);
								pData.boughtGuns.add(g);
								boughtGunInfo.add(g.gunInfo);
								break;
							}
						}
					}else{
						for(Lethal leth : BlockOps.getWeaponManager().getLethalManager().getDefaultLethalArray()){
							if(leth.lethalInfo.character == str.charAt(0)){
								Lethal l = new Lethal(leth.lethalInfo);
								BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, l, false, true, false);
								pData.boughtLethals.add(l);
								boughtLethalInfo.add(leth.lethalInfo);
								break;
							}
						}
						for(Tactical tac : BlockOps.getWeaponManager().getTacticalManager().getDefaultTacticalArray()){
							if(tac.tacticalInfo.character == str.charAt(0)){
								Tactical t = new Tactical(tac.tacticalInfo);
								BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, t, false, true, false);
								pData.boughtTacticals.add(t);
								boughtTacticalInfo.add(tac.tacticalInfo);
								break;
							}
						}
						for(Perk perk : BlockOps.getWeaponManager().getPerkManager().getDefaultPerkArray()){
							if(perk.perkInfo.character == str.charAt(0)){
								Perk p = new Perk(perk.perkInfo);
								BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, p, false, true, false);
								pData.boughtPerks.add(p);
								boughtPerkInfo.add(perk.perkInfo);
								break;
							}
						}
						for(ScoreStreak streak : BlockOps.getWeaponManager().getScoreStreakManager().getDefaultScoreStreakArray()){
							if(streak.scoreStreakInfo.character == str.charAt(0)){
								ScoreStreak st = new ScoreStreak(streak.scoreStreakInfo);
								BlockOps.getWeaponManager().getScoreStreakManager().setScoreStreakItemStack(pData, st, true, false);
								pData.boughtScoreStreaks.add(st);
								break;
							}
						}
					}
				}
				// set unbought itemstacks
				for(GunInfo gunInfo : GunInfo.values()){
					if(!(boughtGunInfo.contains(gunInfo))){
						Gun g = new Gun(gunInfo);
						BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, g, false, false, false);
						pData.unboughtGuns.add(g);
					}
				}
				for(LethalInfo lethInfo : LethalInfo.values()){
					if(!(boughtLethalInfo.contains(lethInfo)) && (lethInfo != LethalInfo.NONE)){
						Lethal leth = new Lethal(lethInfo);
						BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, leth, false, false, false);
						pData.unboughtLethals.add(leth);
					}
				}
				for(TacticalInfo tacInfo : TacticalInfo.values()){
					if(!(boughtTacticalInfo.contains(tacInfo)) && (tacInfo != TacticalInfo.NONE)){
						Tactical tac = new Tactical(tacInfo);
						BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, tac, false, false, false);
						pData.unboughtTacticals.add(tac);
					}
				}
				for(PerkInfo perkInfo : PerkInfo.values()){
					if(!(boughtPerkInfo.contains(perkInfo)) && (perkInfo != PerkInfo.NONE)){
						Perk perk = new Perk(perkInfo);
						BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, perk, false, false, false);
						pData.unboughtPerks.add(perk);
					}
				}
				for(ScoreStreak streak : BlockOps.getWeaponManager().getScoreStreakManager().getDefaultScoreStreakArray()){
					if(streak.scoreStreakInfo.character == data[5].charAt(0)){
						ScoreStreak st = new ScoreStreak(streak.scoreStreakInfo);
						pData.scoreStreak = st;
						BlockOps.getWeaponManager().getScoreStreakManager().setScoreStreakItemStack(pData, st, false, false);
						break;
					}
				}
				String[] classes = new String[]{data[6],data[7],data[8],data[9]}; // class data
				byte number = 0;
				for(String clas : classes){   // FOR CLASSES
					GunClass gClass;
					if(clas.equals("!")){
						gClass = null;
					}else{
						gClass = new GunClass();
						for(String items : clas.split("-")){
							if(items.contains("[")){
								char[] chars = items.toCharArray();
								Gun g = null;
								for(Gun gunLoop : BlockOps.getWeaponManager().getGunManager().getDefaultGunArray()){ // looping through guns
									if(gunLoop.gunInfo.character == chars[0]){
										g = gunLoop.clone();
									}
								}
								for(Attachment attach : BlockOps.getWeaponManager().getAttachmentManager().getDefaultAttachmentArray()){ // looping through attachments
									if(attach.attachmentInfo.character == chars[2]){
										g.attachment1 = new Attachment(attach.attachmentInfo);
										switch(g.attachment1.attachmentInfo){ // add their bonus affects to gun
											case SIGHT: g.Paccuracy=1; break;
											case FMJ: g.Pdamage=1; break;
											case LONG_BARREL: g.Prange=1; break;
											case RAPID_FIRE: g.PfireRate=1; break;
											default: break;
										}
										BlockOps.getWeaponManager().getAttachmentManager().setAttachmentItemStack(pData, g.attachment1, false, false);
									}else if(attach.attachmentInfo.character == chars[3]){
										g.attachment2 =new Attachment(attach.attachmentInfo);
										switch(g.attachment2.attachmentInfo){ // add their bonus affects to gun
											case SIGHT: g.Paccuracy=1; break;
											case FMJ: g.Pdamage=1; break;
											case LONG_BARREL: g.Prange=1; break;
											case RAPID_FIRE: g.PfireRate=1; break;
											default: break;
										}
										BlockOps.getWeaponManager().getAttachmentManager().setAttachmentItemStack(pData, g.attachment2, false, false);
									}
								}
								for(Gun boughtGun : pData.boughtGuns){ // Set g.gunKills
									if(boughtGun.gunInfo == g.gunInfo){
										g.gunKills = boughtGun.gunKills;
									}
								}
								BlockOps.getWeaponManager().getGunManager().setGunItemStack(pData, g, true, true, false);
								if(gClass.primary == null){ // Assignment inside gun Class
									gClass.primary = g;
								}else{
									gClass.secondary = g;
								}
							}else{
								char[] chars = items.toCharArray();
								if(chars.length >= 2){
									for(Lethal leth : BlockOps.getWeaponManager().getLethalManager().getDefaultLethalArray()){
										if(leth.lethalInfo.character == chars[0]){
											if(gClass.lethal1 == null) gClass.lethal1 = new Lethal(leth.lethalInfo);
											BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, gClass.lethal1, true, true, false);
										}else if(leth.lethalInfo.character == chars[1]){
											if(gClass.lethal2 == null) gClass.lethal2 = new Lethal(leth.lethalInfo);
											BlockOps.getWeaponManager().getLethalManager().setLethalItemStack(pData, gClass.lethal2, true, true, false);
										}
									}
									for(Tactical tac : BlockOps.getWeaponManager().getTacticalManager().getDefaultTacticalArray()){
										if(tac.tacticalInfo.character == chars[0]){
											if(gClass.tactical1 == null) gClass.tactical1 = new Tactical(tac.tacticalInfo);
											BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, gClass.tactical1, true, true, false);
										}else if(tac.tacticalInfo.character == chars[1]){
											if(gClass.tactical2 == null) gClass.tactical2 = new Tactical(tac.tacticalInfo);
											BlockOps.getWeaponManager().getTacticalManager().setTacticalItemStack(pData, gClass.tactical2, true, true, false);
										}
									}
								}else{
									for(Perk perk : BlockOps.getWeaponManager().getPerkManager().getDefaultPerkArray()){
										if(perk.perkInfo.character == chars[0]){
											if(gClass.perk == null) gClass.perk = new Perk(perk.perkInfo);
											BlockOps.getWeaponManager().getPerkManager().setPerkItemStack(pData, gClass.perk, true, true, false);
										}
									}
								}
							}
						}
					}
					number++;
					if(number == 1) pData.class1 = gClass;
					if(number == 2) pData.class2 = gClass;
					if(number == 3) pData.class3 = gClass;
					if(number == 4) pData.class4 = gClass;
				}
				pData.time = Integer.valueOf(data[10]); // Time played in game
				// Per Game variables.~!
				String tdmData = data[11];
				byte index = 0;
				for(String k : tdmData.split(",")){
					index++;
					if(index == 1){
						pData.kills = Integer.valueOf(k);
					}else if(index == 2){
						pData.deaths = Integer.valueOf(k);
					}else if(index == 3){
						pData.wins = Integer.valueOf(k);
					}else{
						pData.losses = Integer.valueOf(k);
					}
				}
				this.methodsToExecute();
			}
		}else{
			tries++;
		}
		if(tries >= 5){
			new BukkitRunnable() {
				public void run() {
					p.kickPlayer(ChatColor.RED + "Could not get your data in Time!\nPlease Re-log :)");
				}
			}.runTask(BlockOps.getInstance());
			task.cancel();
			jedis.close();
		}
	}
	
	
	public void methodsToExecute(){   // PUT METHODS TO EXECUTE AFTER DATA IS FETCHED HERE!*!*
		new BukkitRunnable() {
			@Override
			public void run() {
				BlockOps.getDataManager().getDataHashMap().put(p.getUniqueId(), pData);
				BlockOps.getWeaponGUI().setClassItemStacks(pData);
				BlockOps.getWeaponGUI().setGUILevelItemStack(pData);
			}
		}.runTask(BlockOps.getInstance());
	}
}



