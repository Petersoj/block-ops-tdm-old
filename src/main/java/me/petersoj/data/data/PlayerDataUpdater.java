package me.petersoj.data.data;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.petersoj.game.BlockOps;
import me.petersoj.weapons.grenades.Lethal;
import me.petersoj.weapons.grenades.Tactical;
import me.petersoj.weapons.guns.Gun;
import me.petersoj.weapons.guns.GunClass;
import me.petersoj.weapons.perk.Perk;
import me.petersoj.weapons.scorestreak.ScoreStreak;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class PlayerDataUpdater implements Runnable{

	PlayerData pData;
	String uuid;
	
	public PlayerDataUpdater(Player p){
		this.pData = BlockOps.getDataManager().getPlayerData(p);
		uuid = p.getUniqueId().toString();
	}
	
	public void update(){
		Bukkit.getServer().getScheduler().runTaskAsynchronously(BlockOps.getInstance(), this);
	}
	public void run(){
		StringBuilder sBuild = new StringBuilder();
		sBuild.append(System.currentTimeMillis() +"|");
		sBuild.append(pData.rank.name()+"|");
		sBuild.append(pData.xp + "|");
		sBuild.append(pData.coins + "|");
		for(Gun gun : pData.boughtGuns){
			sBuild.append(gun.gunInfo.character + "[" + gun.gunKills + "-");
		}
		for(Lethal leth : pData.boughtLethals){
			sBuild.append(leth.lethalInfo.character + "-");
		}
		for(Tactical tac : pData.boughtTacticals){
			sBuild.append(tac.tacticalInfo.character + "-");
		}
		for(Perk perk : pData.boughtPerks){
			sBuild.append(perk.perkInfo.character + "-");
		}
		for(ScoreStreak streak : pData.boughtScoreStreaks){
			sBuild.append(streak.scoreStreakInfo.character + "-");
		}
		sBuild.append("|" +pData.scoreStreak.scoreStreakInfo.character + "|");
		for(GunClass gClass : new GunClass[]{pData.class1, pData.class2, pData.class3, pData.class4}){
			if(gClass == null){
				sBuild.append("!|");
			}else{
				sBuild.append(gClass.primary.gunInfo.character + "[" + gClass.primary.attachment1.attachmentInfo.character + gClass.primary.attachment2.attachmentInfo.character + "-");
				sBuild.append(gClass.secondary.gunInfo.character + "[" + gClass.secondary.attachment1.attachmentInfo.character + gClass.secondary.attachment2.attachmentInfo.character + "-");
				sBuild.append(String.valueOf(gClass.lethal1.lethalInfo.character) + gClass.lethal2.lethalInfo.character + '-');
				sBuild.append(String.valueOf(gClass.tactical1.tacticalInfo.character) + gClass.tactical2.tacticalInfo.character + '-');
				sBuild.append(gClass.perk.perkInfo.character + "|");
			}
		}
		sBuild.append(pData.time + "|");
		sBuild.append(pData.kills + "," + pData.deaths + "," + pData.wins + "," + pData.losses);
		try{
			Jedis jedis = BlockOps.getDataManager().getJedisObj();
			jedis.set(uuid, sBuild.toString());
		}catch(JedisException e){
			
		}
	}
	
}
