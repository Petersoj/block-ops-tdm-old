package me.petersoj.data.data;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import me.petersoj.game.BlockOps;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class PlayerDataManager implements Listener {
	
	private HashMap<UUID, PlayerData> playerData = new HashMap<UUID, PlayerData>();
	private JedisPool pool;
	
	
	public PlayerDataManager(){
		Bukkit.getPluginManager().registerEvents(this, BlockOps.getInstance());
		ClassLoader previous = Thread.currentThread().getContextClassLoader();
		Thread.currentThread().setContextClassLoader(PlayerDataManager.class.getClassLoader());
		pool = new JedisPool("10.0.1.23", 6379);
		Thread.currentThread().setContextClassLoader(previous);
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent e){    // MAIN JOIN EVENT!!!**
		Player p = e.getPlayer();
		e.setJoinMessage(BlockOps.getMiscMethods().getFormedString("Join", p.getName()));
		PlayerDataFetcher pfetch = new PlayerDataFetcher(p);
		pfetch.executeMethods();
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		e.setQuitMessage(BlockOps.getMiscMethods().getFormedString("Quit", p.getName()));
		if(playerData.containsKey(p.getUniqueId())){
			PlayerDataUpdater update = new PlayerDataUpdater(p);
			update.update();
		}
	}
	public HashMap<UUID, PlayerData> getDataHashMap(){
		return playerData;
	}
	public JedisPool getJedisPool(){
		return pool;
	}
	public Jedis getJedisObj(){
		return pool.getResource();
	}
	
	public PlayerData getPlayerData(Player p){
		return playerData.get(p.getUniqueId());
	}
	
	public int getPlayerLevel(int xp){
		//return (int)(Math.sqrt(Double.valueOf(((xp/10)+1.4)/1.4)));
		return (int)((Math.sqrt((xp/10)+1.4))/1.183215957);
	}
}







