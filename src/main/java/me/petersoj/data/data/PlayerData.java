package me.petersoj.data.data;

import java.util.ArrayList;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.petersoj.misc.Enums.Rank;
import me.petersoj.misc.Enums.Teams;
import me.petersoj.weapons.grenades.Lethal;
import me.petersoj.weapons.grenades.Tactical;
import me.petersoj.weapons.guns.Gun;
import me.petersoj.weapons.guns.GunClass;
import me.petersoj.weapons.perk.Perk;
import me.petersoj.weapons.scorestreak.ScoreStreak;

public class PlayerData {
	
	public Rank rank;
	public int level,xp,coins,time;
	public int kills,deaths,wins,losses; // PER GAME variables
	public Teams team;
	
	public GunClass selectedClass;
	public GunClass class1;
	public GunClass class2;
	public GunClass class3;
	public GunClass class4;
	public ScoreStreak scoreStreak;
	
	public ArrayList<Gun> boughtGuns;
	public ArrayList<Perk> boughtPerks;
	public ArrayList<Lethal> boughtLethals;
	public ArrayList<Tactical> boughtTacticals;
	public ArrayList<ScoreStreak> boughtScoreStreaks;
	
	public ArrayList<Gun> unboughtGuns;
	public ArrayList<Perk> unboughtPerks;
	public ArrayList<Lethal> unboughtLethals;
	public ArrayList<Tactical> unboughtTacticals;
	
	public Inventory inv6;
	public Inventory inv4;
	public ItemStack GUILevelBar,Coins;
	public byte guiByte;
	
	//Other variables
	public boolean reloading, canShoot;
	
	
	public PlayerData(){
		boughtGuns = new ArrayList<Gun>();
		boughtPerks = new ArrayList<Perk>();
		boughtLethals = new ArrayList<Lethal>();
		boughtTacticals = new ArrayList<Tactical>();
		boughtScoreStreaks = new ArrayList<ScoreStreak>();
		unboughtGuns = new ArrayList<Gun>();
		unboughtPerks = new ArrayList<Perk>();
		unboughtLethals = new ArrayList<Lethal>();
		unboughtTacticals = new ArrayList<Tactical>();
		guiByte = 0;
	}

}
