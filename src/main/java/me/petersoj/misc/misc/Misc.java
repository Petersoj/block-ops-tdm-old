package me.petersoj.misc.misc;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import me.petersoj.game.BlockOps;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerListHeaderFooter;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle.EnumTitleAction;
import net.minecraft.server.v1_10_R1.PlayerConnection;

public class Misc implements Listener{
	
	public Misc(){
		Bukkit.getServer().getPluginManager().registerEvents(this, BlockOps.getInstance());
	}
	
	//TODO Default Server protection methods 
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		//e.setCancelled(true);
	}
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e){
		//e.setCancelled(true);
	}
	
	
	public String getFormedString(String prefix, String str){
		return ChatColor.GOLD + "<" + prefix + "> " + ChatColor.GRAY + str;
	}
	
	
	public void sendActionbar(Player player, String message) {
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\":\"" + message + "\"}");
		PacketPlayOutChat packet = new PacketPlayOutChat(icbc, (byte) 2);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	private void sendOneTitle(Player player, EnumTitleAction titleType, String text, int fadeInTime, int showTime, int fadeOutTime) {
        IChatBaseComponent chatTitle = ChatSerializer.a("{\"text\": \"" + text + "\"}");
        if(titleType != EnumTitleAction.TITLE && titleType != EnumTitleAction.SUBTITLE) return;
        PacketPlayOutTitle title = new PacketPlayOutTitle(titleType, chatTitle);
        PacketPlayOutTitle length = new PacketPlayOutTitle(fadeInTime, showTime ,fadeOutTime);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(title);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(length);
    }
	public void sendTitle(Player player, String title, String subtitle, int fadeIn, int showTime, int fadeOut){
		this.sendOneTitle(player, EnumTitleAction.TITLE, title, fadeIn, showTime, fadeOut);
		this.sendOneTitle(player, EnumTitleAction.SUBTITLE, subtitle, fadeIn, showTime, fadeOut);
	}
	
	public void sendHeaderAndFooter(Player p, String head, String foot) {
        CraftPlayer craftplayer = (CraftPlayer) p;
        PlayerConnection connection = craftplayer.getHandle().playerConnection;
        IChatBaseComponent header = ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', head) + "\"}");
        IChatBaseComponent footer = ChatSerializer.a("{\"text\": \"" + ChatColor.translateAlternateColorCodes('&', foot) + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try {
            Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, header);
            headerField.setAccessible(!headerField.isAccessible());

            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, footer);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        }
        connection.sendPacket(packet);
    }

}
