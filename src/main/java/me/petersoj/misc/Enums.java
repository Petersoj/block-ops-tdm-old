package me.petersoj.misc;

public class Enums {
	
	public enum Teams{
		RED, BLUE, ORANGE
	}
	
	public enum Rank{
		PLASMA, YOUTUBE, MOD, MANAGER, DEV, OWNER
	}
	
	
	//TODO ADD LORE AND DISPLAY NAME TO REST OF ENUMS ***  AND ITEM FLAGS
	/*
	 * public enum Attachment{
		NONE('!', "None", "", 4), STOCK('t', "Stock", "", 4), QUICKDRAW('q', "Quickdraw", "", 4), SIGHT('s', "Sight", "", 4), EXTENDED_MAGS('e', "Extended Mags", "", 4), FAST_MAGS('f', "Fast Mags", "", 4), 
		FMJ('j', "FMJ", "", 4),LONG_BARREL('l', "Long Barrel", "", 4), RAPID_FIRE('r', "Rapid Fire", "", 4), LASER_SIGHT('z',"Laser Sight", "", 4),KNOCKBACK('k', "Knockback", "", 4);
		public char cha;
		public String name;
		public ItemStack attachmentItem;
		private Attachment(char cha, String name, String desciption, int durability){
			this.cha = cha;
			this.name = name;
			this.attachmentItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)durability);
		}
	}
	public enum Lethal{
		NONE('!', "None", "", 5),FRAG('m', "Frag", "Cook", 5),SEMTEX('n', "Semtex", "Sticky", 5);
		public final char cha;
		public String name;
		public ItemStack lethalItem;
		private Lethal(char cha, String name, String desciption, int durability){
			this.cha = cha;
			this.name= name;
			this.lethalItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)durability);
		}
	}
	public enum Tactical{
		NONE('!', "None", "", 4),STUN_GRENADE('o', "Stun Grenade", "", 4), FLASH_BANG('p', "Flash Bang", "", 4);
		public final char cha;
		public String name;
		public ItemStack tacticalItem;
		private Tactical(char cha, String name, String desciption, int durability){
			this.cha = cha;
			this.name = name;
			this.tacticalItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)durability);
		}
	}
	public enum Perk{
		NONE('!', "None", "", 4), MARATHON('q', "Marathon", "", 4), SCAVENGER('r', "Scavenger", "", 4), MARTYRDOM('s', "Martyrdom", "", 4);
		public char cha;
		public String name;
		public ItemStack perkItem;
		private Perk(char cha, String name, String desciption, int durability){
			this.cha = cha;
			this.name= name;
			this.perkItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)durability);
		}
	}
	public enum ScoreStreak{
		NONE('!', "None", "", 4), HOUND_STRIKE('t', "Hound Strike", "", 4), PREDITOR_MISSILE('u', "Preditor Missile", "", 4), CHOPPER_INCURSION('v', "Chopper Incursion", "", 4), ADVANED_UAV('w', "Advanced UAV", "", 4);
		public char cha;
		public String name;
		public ItemStack scoreStreakItem;
		private ScoreStreak(char cha, String name, String desciption, int durability){
			this.cha = cha;
			this.name = name;
			this.scoreStreakItem = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)durability);
		}
	}
	 */
}
