package me.petersoj.game;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.WorldCreator;
import org.bukkit.attribute.Attribute;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.craftbukkit.v1_10_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import com.mojang.authlib.GameProfile;

import me.petersoj.data.data.PlayerData;
import me.petersoj.data.data.PlayerDataManager;
import me.petersoj.display.gui.WeaponGUI;
import me.petersoj.display.hud.ScoreBoard;
import me.petersoj.misc.misc.Misc;
import me.petersoj.weapons.WeaponManager;
import net.minecraft.server.v1_10_R1.DataWatcher;
import net.minecraft.server.v1_10_R1.DataWatcherObject;
import net.minecraft.server.v1_10_R1.DataWatcherRegistry;
import net.minecraft.server.v1_10_R1.EntityPlayer;
import net.minecraft.server.v1_10_R1.MinecraftServer;
import net.minecraft.server.v1_10_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_10_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_10_R1.PlayerConnection;
import net.minecraft.server.v1_10_R1.PlayerInteractManager;
import net.minecraft.server.v1_10_R1.WorldServer;

public class BlockOps extends JavaPlugin implements Listener{
	
	private static Plugin plugin;
	private static PlayerDataManager dataManager;
	private static WeaponManager weapons;
	private static ScoreBoard scoreboard;
	private static WeaponGUI weaponGui;
	
	private static Misc misc;
	
	@Override
	public void onEnable(){
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.createWorld(new WorldCreator("world2"));
		plugin  = this;
		dataManager = new PlayerDataManager();
		weaponGui = new WeaponGUI();
		weapons = new WeaponManager();
		scoreboard = new ScoreBoard();
		misc = new Misc();
	}
	@Override
	public void onDisable(){
		getDataManager().getJedisPool().close();
	}
	
	public static Plugin getInstance(){
		return plugin;
	}
	public static WeaponManager getWeaponManager(){
		return weapons;
	}
	public static PlayerDataManager getDataManager(){
		return dataManager;
	}
	public static Misc getMiscMethods(){
		return misc;
	}
	public static ScoreBoard getScoreboard(){
		return scoreboard;
	}
	public static WeaponGUI getWeaponGUI(){
		return weaponGui;
	}
	
	//4-(4/sqrt(4*3))
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		if(e.getMessage().equalsIgnoreCase("/dod")){
			Bukkit.broadcastMessage(String.valueOf(e.getPlayer().hasLineOfSight(Bukkit.getPlayer("Mateothebeast97"))));
		}
		if(e.getMessage().equalsIgnoreCase("/look")){
			Location eye = e.getPlayer().getEyeLocation();
			Location mEye = Bukkit.getPlayer("Mateothebeast97").getEyeLocation();
			
			Vector vec = mEye.toVector().subtract(eye.toVector()).normalize();
			double dot = vec.dot(eye.getDirection().normalize());
			Bukkit.broadcastMessage(String.valueOf(dot));
		}
		if(e.getMessage().equalsIgnoreCase("/taco")){
			DataWatcher watcher = new DataWatcher(((CraftPlayer)e.getPlayer()).getHandle());
			watcher.register(new DataWatcherObject<>(6, DataWatcherRegistry.a), (byte) 1);
			for(Player p : Bukkit.getOnlinePlayers()){
				((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(((CraftPlayer)e.getPlayer()).getHandle().getId(), watcher, true));
			}
		}
		if(e.getMessage().equalsIgnoreCase("/up")){
			Inventory inv = Bukkit.createInventory(null, 54, ChatColor.GOLD + "Gun Classes");
			ItemStack it = new ItemStack(Material.DIAMOND_PICKAXE);
			ItemMeta im = it.getItemMeta();
			im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
			im.setDisplayName(ChatColor.WHITE + "HEY");
			it.setItemMeta(im);
			it.addEnchantment(Enchantment.DIG_SPEED, 1);
			inv.addItem(it);
			e.getPlayer().openInventory(inv);
			it.removeEnchantment(Enchantment.DIG_SPEED);
			e.getPlayer().getInventory().addItem(it);
		}
		if(e.getMessage().startsWith("/speed")){
			e.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(Double.valueOf(e.getMessage().split(" ")[1]));;
		}
		if(e.getMessage().startsWith("/walk")){
			e.getPlayer().setWalkSpeed(Float.valueOf(e.getMessage().split(" ")[1]));
		}
		if(e.getMessage().equalsIgnoreCase("/bubble")){
			new BukkitRunnable() {
				@Override
				public void run() {
					Location loc = e.getPlayer().getEyeLocation().getDirection().multiply(2).toLocation(e.getPlayer().getWorld());
					e.getPlayer().spawnParticle(Particle.WATER_BUBBLE, loc.add(e.getPlayer().getEyeLocation()), 1, 0, 0, 0, 1);
				}
			}.runTaskTimer(this, 0, 3);
		}
		if(e.getMessage().equalsIgnoreCase("/ta")){
			MinecraftServer nmsServer = ((CraftServer) Bukkit.getServer()).getServer();
			WorldServer nmsWorld = ((CraftWorld) Bukkit.getWorlds().get(0)).getHandle();
			GameProfile profile = new GameProfile(UUID.randomUUID(), ChatColor.GREEN + "Dead body :P");
			profile.getProperties().putAll("textures", ((CraftPlayer)e.getPlayer()).getProfile().getProperties().get("textures"));
			EntityPlayer npc = new EntityPlayer(nmsServer, nmsWorld, profile, new PlayerInteractManager(nmsWorld));
			Location loc = e.getPlayer().getLocation();
			npc.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
			DataWatcher watcher = new DataWatcher(npc);
			watcher.register(new DataWatcherObject<>(0, DataWatcherRegistry.a), (byte) 0x80);
			for(Player p : Bukkit.getOnlinePlayers()){
				PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
				connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, npc));
				connection.sendPacket(new PacketPlayOutNamedEntitySpawn(npc));
				((CraftPlayer)p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(npc.getId(), watcher, true));
				new BukkitRunnable() {
					@Override
					public void run() {
						connection.sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, npc));
					}
				}.runTaskLater(this, 50L);
			}
		}
		if(e.getMessage().equalsIgnoreCase("/aa")){
			Player p = e.getPlayer();
			Player other = Bukkit.getPlayer("dawici");
			
			Location min = other.getLocation().add(-0.3, 0, -0.3);
			Location max = other.getLocation().add(0.3, 1.62, 0.3);
			
			p.getWorld().spigot().playEffect(min, Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			p.getWorld().spigot().playEffect(max, Effect.FIREWORKS_SPARK, 0, 0, 0, 0, 0, 0, 1, 30);
			
			Bukkit.broadcastMessage("Min: " + min.toVector().toString() + ", Max: " + max.toVector().toString());
			Bukkit.broadcastMessage(p.getLocation().getDirection().add(p.getEyeLocation().toVector()).toString());
			
			boolean axis = p.getLocation().getDirection().add(p.getEyeLocation().toVector()).isInAABB(min.toVector(), max.toVector());
			Bukkit.broadcastMessage(String.valueOf(axis));
		}
		if(e.getMessage().equalsIgnoreCase("/class")){
			PlayerData pData = getDataManager().getPlayerData(e.getPlayer());
			getWeaponGUI().loadMainMenu(pData);
			e.getPlayer().openInventory(pData.inv4);
		}
		
		if(e.getMessage().startsWith("/tw")){
			Bukkit.broadcastMessage(e.getMessage().split(" ")[1]);
			e.getPlayer().teleport(Bukkit.getWorld(e.getMessage().split(" ")[1]).getSpawnLocation());
		}
	}
	
	
	BukkitTask task = null;
	Location loc = null;
	public void onShift(PlayerToggleSneakEvent e){
		Bukkit.broadcastMessage(String.valueOf(e.isSneaking()));
		e.getPlayer().setWalkSpeed((float)0.65);
		if(e.isSneaking()){
			Player p = e.getPlayer();
			loc = p.getLocation();
			Random rand = new Random();
			task = new BukkitRunnable() {
				@Override
				public void run() {
					if(Math.abs(loc.getPitch() - p.getLocation().getPitch()) > 0.7 || Math.abs(loc.getYaw() - p.getLocation().getYaw()) > 0.7){
						Bukkit.broadcastMessage("MORE");
					}else{
						Bukkit.broadcastMessage("Less");
						Vector vec = p.getVelocity();
						Location loc = new Location(p.getWorld(), p.getLocation().getX(), p.getLocation().getY(), p.getLocation().getZ(), 
								p.getLocation().getYaw() - (float)(rand.nextDouble() - rand.nextDouble()), p.getLocation().getPitch() - (float)(rand.nextDouble() - rand.nextDouble()));
						p.teleport(loc);
						p.setVelocity(vec.multiply(1.1));
					}
					loc = p.getLocation();
				}
			}.runTaskTimer(this, 0, 2);
		}else{
			e.getPlayer().setWalkSpeed((float)0.2);
			task.cancel();
		}
		
	}
}
