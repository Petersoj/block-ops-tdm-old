package me.petersoj.weapons.grenades;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class TacticalManager {
	
	public Tactical none,stun,flash;

	public TacticalManager(){
		none=new Tactical(TacticalInfo.NONE);stun = new Tactical(TacticalInfo.STUN); flash = new Tactical(TacticalInfo.FLASH);
		
		this.setTacticalItemStack(null, none, false, false, true);
		this.setTacticalItemStack(null, stun, false, false, true);
		this.setTacticalItemStack(null, flash, false, false, true);
	}
	
	public enum TacticalInfo{
		NONE("None", "", '!', '⣟', 4, 4),
		STUN("Stun Grenade", "Stun", 'o', '⣟', 4, 4),
		FLASH("Flash Grenade", "Flash", 'p', '⣟', 4, 4);
		
		public String name,description;
		public char character,visualCharacter;
		public int unlockLevel,durability;
		private TacticalInfo(String name, String description, char character, char visual, int unlockLevel, int durability){
			this.name = name; this.description = description; this.character = character; this.visualCharacter = visual;
			this.unlockLevel = unlockLevel; this.durability = durability;
		}
	}
	
	public Tactical[] getDefaultTacticalArray(){
		return new Tactical[]{none,stun,flash};
	}
	
	public void setTacticalItemStack(PlayerData pData, Tactical tac, boolean selected, boolean bought, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				tac.itemstack = getTacticalItem(pData, tac, selected, bought, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getTacticalItem(PlayerData pData, Tactical tac, boolean selected, boolean bought, boolean locked){
		if(tac == none){
			return BlockOps.getWeaponGUI().noneItem;
		}
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)tac.tacticalInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + tac.tacticalInfo.name);
		List<String> list = new ArrayList<String>();
		list.add("");
		if(selected){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + tac.tacticalInfo.name + ChatColor.BLUE + " - Tactical");	
		}
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + tac.tacticalInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at level " + ChatColor.WHITE + tac.tacticalInfo.unlockLevel);
			list.add("");
		}else if(!bought){
			meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + tac.tacticalInfo.name
					+ ChatColor.WHITE + " - " + ChatColor.GOLD + "You have " + /*TODO PUT COIN EMOJI HERE*/String.valueOf(pData.coins) +" coins");
			list.add(ChatColor.GOLD + /*TODO PUT COIN EMOJI HERE*/"1 Coins");
			list.add("");
		}
		list.add(ChatColor.WHITE + tac.tacticalInfo.description);
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
}
