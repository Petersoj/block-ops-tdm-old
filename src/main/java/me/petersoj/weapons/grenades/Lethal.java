package me.petersoj.weapons.grenades;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.grenades.LethalManager.LethalInfo;

public class Lethal {
	
	public LethalInfo lethalInfo;
	public ItemStack itemstack;
	
	public Lethal(LethalInfo info){
		this.lethalInfo = info;
	}
}
