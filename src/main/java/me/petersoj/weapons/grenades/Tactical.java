package me.petersoj.weapons.grenades;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.grenades.TacticalManager.TacticalInfo;

public class Tactical {

	public TacticalInfo tacticalInfo;
	public ItemStack itemstack;
	
	public Tactical(TacticalInfo info){
		this.tacticalInfo = info;
	}
}
