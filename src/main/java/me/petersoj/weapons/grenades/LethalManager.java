package me.petersoj.weapons.grenades;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class LethalManager {
	
	public Lethal none,frag,semtex;
	
	public LethalManager(){
		none=new Lethal(LethalInfo.NONE); frag = new Lethal(LethalInfo.FRAG); semtex=new Lethal(LethalInfo.SEMTEX);
		
		this.setLethalItemStack(null, none, false, false, true);
		this.setLethalItemStack(null, frag, false, false, true);
		this.setLethalItemStack(null, semtex, false, false, true);
	}
	
	public enum LethalInfo{
		NONE("None", "", '!', '⣟', 0, 4),
		FRAG("Frag", "Cookable by holding right click.", 'm', '⣟', 5, 4),
		SEMTEX("Semtex", "Sticks to surfaces", 'n', '⣟', 5, 4);
		
		public String name,description;
		public char character,visualCharacter;
		public int unlockLevel,durability;
		private LethalInfo(String name, String description, char character, char visual, int unlockLevel, int durability){
			this.name = name; this.description = description; this.character = character; this.visualCharacter = visual;
			this.unlockLevel = unlockLevel; this.durability = durability;
		}
	}
	
	public Lethal[] getDefaultLethalArray(){
		return new Lethal[]{none,frag,semtex};
	}
	
	public void setLethalItemStack(PlayerData pData, Lethal leth, boolean selected, boolean bought, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				leth.itemstack = getLethalItem(pData, leth, selected, bought, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getLethalItem(PlayerData pData, Lethal leth, boolean selected, boolean bought, boolean locked){
		if(leth == none){
			return BlockOps.getWeaponGUI().noneItem;
		}
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)leth.lethalInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + leth.lethalInfo.name);
		List<String> list = new ArrayList<String>();
		list.add("");
		if(selected){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + leth.lethalInfo.name + ChatColor.BLUE + " - Lethal");
		}
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + leth.lethalInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at level " + ChatColor.WHITE + leth.lethalInfo.unlockLevel);
			list.add("");
		}else if(!bought){
			meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + leth.lethalInfo.name
					+ ChatColor.WHITE + " - " + ChatColor.GOLD + "You have " + /*TODO PUT COIN EMOJI HERE*/String.valueOf(pData.coins) +" coins");
			list.add(ChatColor.GOLD + /*TODO PUT COIN EMOJI HERE*/"1 Coin");
			list.add("");
		}
		list.add(ChatColor.WHITE + leth.lethalInfo.description);
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
}
