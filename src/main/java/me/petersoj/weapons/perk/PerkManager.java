package me.petersoj.weapons.perk;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class PerkManager {
	
	public Perk none,afterburner,fastHands,tacticalMask,flakJacket,marathon,scavenger,martyrdom;
	
	public PerkManager(){
		none=new Perk(PerkInfo.NONE);afterburner=new Perk(PerkInfo.AFTERBURNER);fastHands=new Perk(PerkInfo.FAST_HANDS);
		tacticalMask=new Perk(PerkInfo.TACTICALMASK);flakJacket=new Perk(PerkInfo.FLAKJACKET);marathon=new Perk(PerkInfo.MARATHON);
		scavenger=new Perk(PerkInfo.SCAVENGER);martyrdom=new Perk(PerkInfo.MARTYRDOM);
		
		this.setPerkItemStack(null, none, false, false, true);
		this.setPerkItemStack(null, marathon, false, false, true);
		this.setPerkItemStack(null, scavenger, false, false, true);
		this.setPerkItemStack(null, martyrdom, false, false, true);
	}
	
	public enum PerkInfo{
		NONE("None", "", '!', 0, 0),
		AFTERBURNER("AfterBurner", "Recharge your thrust bar quicker", 'x', 0, 0),
		FAST_HANDS("Fast Hands", "Swap weapons quicker.", 'y', 0, 0),
		TACTICALMASK("Tactical Mask", "Take less effect from tacticals grenades.", 'z', 0, 0),
		FLAKJACKET("Flak Jacket", "Take less damage from explosives.", 'a', 0, 0),
		MARATHON("Marathon", "Frag Bro", 'q', 4, 4),
		SCAVENGER("Scavenger", "Semtex Bro", 'r', 4, 4),
		MARTYRDOM("Martyrdom", "Semtex Bro", 's', 4, 4);
		
		public String name, description;
		public char character;
		public int unlockLevel,durability;
		private PerkInfo(String name, String description, char character, int unlockLevel, int durability){
			this.name = name; this.description = description; this.character = character;
			this.unlockLevel = unlockLevel; this.durability = durability;
		}
	}
	
	public Perk[] getDefaultPerkArray(){
		return new Perk[]{none,marathon,scavenger,martyrdom};
	}
	

	public void setPerkItemStack(PlayerData pData,Perk perk, boolean selected, boolean bought, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				perk.itemstack = getPerkItem(pData, perk, selected, bought, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getPerkItem(PlayerData pData, Perk perk, boolean selected, boolean bought, boolean locked){
		if(perk == none){
			return BlockOps.getWeaponGUI().noneItem;
		}
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)perk.perkInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + perk.perkInfo.name);
		List<String> list = new ArrayList<String>();
		list.add("");
		if(selected){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + perk.perkInfo.name + ChatColor.BLUE + " - Perk");
		}
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + perk.perkInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at level " + ChatColor.WHITE + perk.perkInfo.unlockLevel);
			list.add("");
		}else if(!bought){
			meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + perk.perkInfo.name
					+ ChatColor.WHITE + " - " + ChatColor.GOLD + "You have " + /*TODO PUT COIN EMOJI HERE*/String.valueOf(pData.coins) +" coins");
			list.add(ChatColor.GOLD + /*TODO PUT COIN EMOJI HERE*/"1 Coin");
			list.add("");
		}
		list.add(ChatColor.WHITE + perk.perkInfo.description);
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
}
