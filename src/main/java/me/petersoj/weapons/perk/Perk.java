package me.petersoj.weapons.perk;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.perk.PerkManager.PerkInfo;

public class Perk {
	public PerkInfo perkInfo;
	public ItemStack itemstack;
	
	public Perk(PerkInfo info){
		this.perkInfo = info;
	}
}
