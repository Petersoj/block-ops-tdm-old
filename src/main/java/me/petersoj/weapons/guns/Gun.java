package me.petersoj.weapons.guns;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.guns.GunManager.GunInfo;

public class Gun{
	
	public GunInfo gunInfo;
	public byte Pdamage,Paccuracy,Prange,PfireRate;
	public int ammo,mags;
	public Attachment attachment1,attachment2;
	public int gunKills;
	public ItemStack itemStack;
	
	public Gun(GunInfo info){
		this.gunInfo = info;
	}
	
	public Gun clone(){
		Gun g = new Gun(this.gunInfo);
		g.Pdamage = this.Pdamage; g.Paccuracy = this.Paccuracy; g.Prange = this.Prange; g.PfireRate = this.PfireRate;
		g.ammo = this.ammo; g.mags = this.mags;
		g.attachment1 = this.attachment1; g.attachment2 = this.attachment2;
		g.gunKills = this.gunKills;
		g.itemStack = this.itemStack;
		return g;
	}
	
}
