package me.petersoj.weapons.guns;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class Shot implements Runnable{
	
	Player player;
	Player target;
	PlayerData playerData;
	Gun gun;
	BukkitTask task;
	
	int repeat;
	int check; // for run method
	
	public void shoot(Player player, Gun gun){
		playerData = BlockOps.getDataManager().getPlayerData(player);
		if(!(playerData.canShoot) || playerData.reloading){
			return;
		}/*
		else if( CHECK IF NO BULLETS){
			player empty mag sound!
		}*/
		
		this.player = player;
		this.gun = gun;
		this.check = 0;
		int totalRate = 0;//gun.PfireRate+gun.fireRate;
		int rate = (totalRate > 10) ? 10 : Math.round(totalRate/2);
		
		int ts = 0;
		switch(rate){
			case 0:
				return;
			case 1:
				// wait 400 ms
				break;
			case 2:
				// wait 200 ms
				break;
			case 3:
				repeat = 0;
				break;
			case 4:
				repeat = 1;
				ts = 2;
				break;
			case 5:
				repeat = 4;
				ts = 1;
				break;
		}
		double closest = 0.9;
		for(Player p : Bukkit.getOnlinePlayers()){
			if(BlockOps.getDataManager().getPlayerData(p).team != playerData.team){
				Vector vec = p.getLocation().add(0, 0.6, 0).toVector().subtract(player.getEyeLocation().toVector()).normalize();
				double dot = vec.dot(player.getEyeLocation().getDirection().normalize());
				if((dot > closest) && player.hasLineOfSight(p)){
					closest = dot;
					target = p;
				}
			}
		}
		
		// WILL trigger instantly when invoked AND can only be run 4 times
		task = Bukkit.getServer().getScheduler().runTaskTimer(BlockOps.getInstance(), this, 0, ts);
		
		/*
		 *  Location eye = e.getPlayer().getEyeLocation();
			Location mEye = Bukkit.getPlayer("Mateothebeast97").getEyeLocation();
			
			Vector vec = mEye.toVector().subtract(eye.toVector()).normalize();
			double dot = vec.dot(eye.getDirection().normalize());
			
			Vector vector1 = Bukkit.getPlayer("Mateothebeast97").getLocation().add(0, 0.4, 0).toVector().subtract(e.getPlayer().getEyeLocation().toVector()).normalize();
			Random rand = new Random();
			vector1.add(new Vector(rand.nextDouble(), rand.nextDouble(), rand.nextDouble()));
			double secondDot = vector1.dot(e.getPlayer().getLocation().add(0, 5, 0).getDirection().normalize());
			Bukkit.broadcastMessage(String.valueOf("Head: " + dot + "     HIP: " + secondDot));
		 */
	}
	
	
	public void run(){
		
		
		if(check >= repeat){
			task.cancel();
		}
		check++;
	}

}
