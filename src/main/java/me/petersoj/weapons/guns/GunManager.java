package me.petersoj.weapons.guns;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class GunManager implements Listener{
	
	//DEFAULT GUNS
	public Gun puma,R1L,L4R,bithit,T1M2,shred9,shone,A7X,PM3,puzzle,smega,combatKnife;
	public Gun[] unboughtGuns;
	
	public GunManager(){
		Bukkit.getPluginManager().registerEvents(this, BlockOps.getInstance());
		// TODO SET REST OF GUN VALUES!!!!!!!
		puma=new Gun(GunInfo.PUMA);R1L=new Gun(GunInfo.R1L);L4R=new Gun(GunInfo.L4R);bithit=new Gun(GunInfo.BITHIT);
		T1M2=new Gun(GunInfo.T1M2);shred9=new Gun(GunInfo.SHRED_9);shone=new Gun(GunInfo.SHONE);
		A7X=new Gun(GunInfo.A7X);PM3=new Gun(GunInfo.PM3);puzzle=new Gun(GunInfo.PUZZ1E);
		smega=new Gun(GunInfo.SMEGA);combatKnife=new Gun(GunInfo.COMBAT_KNIFE);
		
		this.setGunItemStack(null, puma, false, false, true); this.setGunItemStack(null, R1L, false, false, true); this.setGunItemStack(null, L4R, false, false, true);
		this.setGunItemStack(null, bithit, false, false, true); this.setGunItemStack(null, T1M2, false, false, true); this.setGunItemStack(null, shred9, false, false, true);
		this.setGunItemStack(null, shone, false, false, true); this.setGunItemStack(null, A7X, false, false, true); this.setGunItemStack(null, PM3, false, false, true);
		this.setGunItemStack(null, puzzle, false, false, true); this.setGunItemStack(null, smega, false, false, true); this.setGunItemStack(null, combatKnife, false, false, true);
	}
	
	public enum GunType{
		SMG("SMG"), ASSAULT_RIFLE("Assault Rifle"), SHOTGUN("Shotgun"), SNIPER("Sniper"), PISTOL("Pistol"), SPECIAL("Special");
		public String name;
		private GunType(String str){
			this.name = str;
		}
	}
	
	public enum GunInfo{
		// SMG
		PUMA("Puma", 'a', '⣟', 3, GunType.SMG, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // KUDA
		R1L("R1L", 'b', '⣟', 3, GunType.SMG, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // WEEVIL
		// Assault Rifle
		L4R("L4R", 'c', '⣟', 3, GunType.ASSAULT_RIFLE, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // KN-44			 DEFAULT 
		BITHIT("Bit Hit", '⣟', 'd', 3, GunType.ASSAULT_RIFLE, 3, 3,3,3,3,4,4, 30,3, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // Man-O-War   
		//ShotGun
		T1M2("T1M2", 'e', '⣟', 3, GunType.SHOTGUN, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3),  // KRM-262
		SHRED_9("Shred 9", '⣟', 'f', 3, GunType.SHOTGUN, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // 205 Brecci    
		// Snipers
		SHONE("Shone", 'g', '⣟', 3, GunType.SNIPER, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), //  Drakon  
		A7X("A7X", 'h', '⣟', 3, GunType.SNIPER, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // Locus   
		// Pistol
		PM3("PM3", 'i', '⣟', 3, GunType.PISTOL, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // MR6         		DEFAULT
		PUZZ1E("PUZZ1E", 'j', '⣟', 3, GunType.PISTOL, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // LCAR
		//Special 
		SMEGA("Smega", 'k', '⣟', 3, GunType.SPECIAL, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3), // Rocket Launcher   
		COMBAT_KNIFE("Combat Knife", '⣟', 'l', 3, GunType.SPECIAL, 3, 3,3,3,3, 30,3,4,4, Sound.AMBIENT_CAVE, 3.4,3.4,3.4, 2.4,2.4,2.3); // combat Knife
		
		public String name;
		public char character,visualCharacter;
		public GunType gunType;
		public int durability,unlockLevel,damage,range,fireRate,accuracy,maxBullets,maxMags,reloadTime,cockTime;
		public Sound sound;
		public double firstPersonX,firstPersonY,firstPersonMagnitude,thirdPersonX,thirdPersonY,thirdPersonMagnitude;
		private GunInfo(String name, char charac, char visual, int durability, GunType type, int unlock, int damage, int range, int fireRate, int accuracy, 
				int bullets, int mags, int reload, int cock, Sound sound, double firstX, double firstY, double firstMagnitude, double thirdX, double thirdY, double thirdMagnitude){
			this.name = name; this.character = charac; this.visualCharacter = visual; this.durability = durability; this.gunType = type;this.maxBullets = bullets;
			this.unlockLevel = unlock; this.damage = damage; this.range = range; this.fireRate = fireRate; this.accuracy = accuracy;
			this.maxMags = mags; this.reloadTime = reload; this.cockTime = cock;this.sound = sound; this.firstPersonX = firstX; this.firstPersonY = firstY; 
			this.firstPersonMagnitude = firstMagnitude; this.thirdPersonX = thirdX; this.thirdPersonY = thirdY; this.thirdPersonMagnitude = thirdMagnitude;
		}
	}
	
	@EventHandler
	public void onClick(PlayerInteractEvent e){
		//Player p = e.getPlayer();
		
	}
	public Gun[] getDefaultGunArray(){
		return new Gun[]{puma,R1L,L4R,bithit,T1M2,shred9,shone,A7X,PM3,puzzle,smega,combatKnife};
	}
	
	public Gun[] getSMGGunArray(){
		return new Gun[]{puma,R1L};
	}
	public Gun[] getAssaultGunArray(){
		return new Gun[]{L4R,bithit};
	}
	public Gun[] getShotgunGunArray(){
		return new Gun[]{T1M2,shred9};
	}
	public Gun[] getSniperGunArray(){
		return new Gun[]{shone,A7X};
	}
	public Gun[] getPistolGunArray(){
		return new Gun[]{PM3,puzzle};
	}
	public Gun[] getLauncherGunArray(){
		return new Gun[]{smega};
	}
	public Gun[] getSpecialGunArray(){
		return new Gun[]{combatKnife};
	}
	
	public double getGunLevel(int kills){
		DecimalFormat form = new DecimalFormat("00.#");
		return Double.valueOf(form.format(Math.sqrt(Double.valueOf((kills+1.4)/1.4))));
	}
	
	public void setGunItemStack(PlayerData pData, Gun gun, boolean selected, boolean bought, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				gun.itemStack = getNewGunItem(pData, gun, selected, bought, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getNewGunItem(PlayerData pData, Gun gun, boolean selected, boolean bought, boolean locked){
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)gun.gunInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" +ChatColor.UNDERLINE + "" + ChatColor.BOLD + gun.gunInfo.name);
		List<String> list = new ArrayList<String>();
		list.add(" ");
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" +ChatColor.UNDERLINE + "" + ChatColor.BOLD + gun.gunInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at level " + ChatColor.WHITE + gun.gunInfo.unlockLevel);
		}else if(!bought){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + gun.gunInfo.name
				+ ChatColor.WHITE + " - " + ChatColor.GOLD + "You have " + /*TODO PUT COIN EMOJI HERE*/String.valueOf(pData.coins) +" coins");
			list.add(ChatColor.GOLD + /*TODO PUT COIN EMOJI HERE*/"2 Coins");
		}
		list.add(ChatColor.GRAY + "Type: " + ChatColor.BLUE + gun.gunInfo.gunType.name);
		list.add(ChatColor.GRAY + "Bullets Per Clip: " + ChatColor.BLUE + gun.gunInfo.maxBullets);
		list.add(ChatColor.GRAY + "Mags: " + ChatColor.BLUE + gun.gunInfo.maxMags);
		list.add(" ");
		if(selected){
			if(gun.gunInfo.gunType == GunType.PISTOL || gun.gunInfo.gunType == GunType.SPECIAL){
				meta.setDisplayName(ChatColor.GOLD + "" +ChatColor.UNDERLINE + "" + ChatColor.BOLD + gun.gunInfo.name + ChatColor.BLUE + " - Secondary");
			}else{
				meta.setDisplayName(ChatColor.GOLD + "" +ChatColor.UNDERLINE + "" + ChatColor.BOLD + gun.gunInfo.name + ChatColor.BLUE + " - Primary");
			}
			list.add(ChatColor.GRAY + "Attachments: " + ChatColor.BLUE + gun.attachment1.attachmentInfo.name + ", " + gun.attachment2.attachmentInfo.name);
			list.add(" ");
		}
		if(!locked && bought){
			list.add(ChatColor.GRAY + "Gun Level: " + ChatColor.GOLD + BlockOps.getWeaponManager().getGunManager().getGunLevel(gun.gunKills));
			list.add(" ");
		}
		StringBuilder damageB = new StringBuilder(ChatColor.WHITE.toString());     		// Damage boxes
		for(int a = 1; a<= 10; a++){
			if(a <= gun.gunInfo.damage){
				damageB.append("█");
			}else{
				if(a > gun.gunInfo.damage && a <= gun.Pdamage+gun.gunInfo.damage){
					damageB.append(ChatColor.GREEN + "");
				}
				if(a > gun.gunInfo.damage+gun.Pdamage){
					damageB.append(ChatColor.DARK_GRAY + "");
				}
				damageB.append("█");
			}
		}
		list.add(damageB.toString() + ChatColor.WHITE + " Damage");
		
		StringBuilder rangeB = new StringBuilder(ChatColor.WHITE.toString());     		// range boxes
		for(int a = 1; a<= 10; a++){
			if(a <= gun.gunInfo.range){
				rangeB.append("█");
			}else{
				if(a > gun.gunInfo.range && a <= gun.Prange+gun.gunInfo.range){
					rangeB.append(ChatColor.GREEN + "");
				}
				if(a > gun.gunInfo.range+gun.Prange){
					rangeB.append(ChatColor.DARK_GRAY + "");
				}
				rangeB.append("█");
			}
		}
		list.add(rangeB.toString() + ChatColor.WHITE + " Range");
		
		StringBuilder fireRateB = new StringBuilder(ChatColor.WHITE.toString());     		// Fire Rate boxes
		for(int a = 1; a<= 10; a++){
			if(a <= gun.gunInfo.fireRate){
				fireRateB.append("█");
			}else{
				if(a > gun.gunInfo.fireRate && a <= gun.PfireRate+gun.gunInfo.fireRate){
					fireRateB.append(ChatColor.GREEN + "");
				}
				if(a > gun.gunInfo.fireRate+gun.PfireRate){
					fireRateB.append(ChatColor.DARK_GRAY + "");
				}
				fireRateB.append("█");
			}
		}
		list.add(fireRateB.toString() + ChatColor.WHITE + " Fire Rate");
		
		StringBuilder accuracyB = new StringBuilder(ChatColor.WHITE.toString());     		// Accuracy boxes
		for(int a = 1; a<= 10; a++){
			if(a <= gun.gunInfo.accuracy){
				accuracyB.append("█");
			}else{
				if(a > gun.gunInfo.accuracy && a <= gun.Paccuracy+gun.gunInfo.accuracy){
					accuracyB.append(ChatColor.GREEN + "");
				}
				if(a > gun.gunInfo.accuracy + gun.Paccuracy){
					accuracyB.append(ChatColor.DARK_GRAY + "");
				}
				accuracyB.append("█");
			}
		}
		list.add(accuracyB.toString() + ChatColor.WHITE + " Accuracy");
		
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
}
