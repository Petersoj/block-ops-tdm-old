package me.petersoj.weapons.guns;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class AttachmentManager {
	
	public Attachment none,stock,quickdraw,sight,extendedMags,fastMags,fmj,longBarrel,rapidFire,laserSight,knockback;
	public Attachment[] unlockedAttachments;

	public AttachmentManager(){
		none=new Attachment(AttachmentInfo.NONE);stock=new Attachment(AttachmentInfo.STOCK);quickdraw=new Attachment(AttachmentInfo.QUICKDRAW);
		sight=new Attachment(AttachmentInfo.SIGHT);extendedMags=new Attachment(AttachmentInfo.EXTENDED_MAGS);fastMags=new Attachment(AttachmentInfo.FAST_MAGS);
		fmj=new Attachment(AttachmentInfo.FMJ);longBarrel=new Attachment(AttachmentInfo.LONG_BARREL);rapidFire=new Attachment(AttachmentInfo.RAPID_FIRE);
		laserSight=new Attachment(AttachmentInfo.LASER_SIGHT);knockback=new Attachment(AttachmentInfo.KNOCKBACK);
		
		this.setAttachmentItemStack(null, none, true, true);
		this.setAttachmentItemStack(null, stock, true, true);
		this.setAttachmentItemStack(null, quickdraw, true, true);
		this.setAttachmentItemStack(null, sight, true, true);
		this.setAttachmentItemStack(null, extendedMags, true, true);
		this.setAttachmentItemStack(null, fastMags, true, true);
		this.setAttachmentItemStack(null, fmj, true, true);
		this.setAttachmentItemStack(null, longBarrel, true, true);
		this.setAttachmentItemStack(null, rapidFire, true, true);
		this.setAttachmentItemStack(null, laserSight, true, true);
		this.setAttachmentItemStack(null, knockback, true, true);
		
		//unlockedAttachments = new Attachment[]{none.clone(),stock.clone(),quickdraw.clone(),sight.clone(),extendedMags.clone(),fastMags.clone(),
//			fmj.clone(),longBarrel.clone(),rapidFire.clone(),laserSight.clone(),knockback.clone()};
		//for(Attachment at : unlockedAttachments){
			//this.setAttachmentItemStack(null, at, true, false);
		//}
	}
	
	public enum AttachmentInfo{
		NONE("None", "", '!', 0, 0), STOCK("Stock", "St", 't', 4, 4), QUICKDRAW("Quickdraw", "St", 'q', 4, 4),
		SIGHT("Sight", "St", 's', 4, 4), EXTENDED_MAGS("Extended Mags", "St", 'e', 4, 4), FAST_MAGS("Fast Mags", "St", 'f', 4, 4),
		FMJ("FMJ", "St", 'j', 4, 4), LONG_BARREL("Long Barrel", "St", 'l', 4, 4), RAPID_FIRE("Rapid Fire", "St", 'r', 4, 4),
		LASER_SIGHT("Laser Sight", "St", 'z', 4, 4), KNOCKBACK("Knockback", "St", 'k', 4, 4);
		
		public String name, description;
		public char character;
		public int weaponUnlockLevel,durability;
		private AttachmentInfo(String name, String description, char character, int weaponUnlockLevel, int durability){
			this.name = name; this.description = description; this.character = character;
			this.weaponUnlockLevel = weaponUnlockLevel; this.durability = durability;
		}
	}
	
	public Attachment[] getDefaultAttachmentArray(){
		return new Attachment[]{none,stock,quickdraw,sight,extendedMags,fastMags,fmj,longBarrel,rapidFire,laserSight,knockback};
	}
	public Attachment[] getUnlockedAttachmentArray(){
		return this.unlockedAttachments;
	}
	
	public void setAttachmentItemStack(PlayerData pData,Attachment attach, boolean show, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				attach.itemstack = getAttachmentItem(pData, attach, show, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getAttachmentItem(PlayerData pData, Attachment attach, boolean showGun, boolean locked){
		if(attach == none){
			return BlockOps.getWeaponGUI().noneItem;
		}
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)attach.attachmentInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + attach.attachmentInfo.name);
		List<String> list = new ArrayList<String>();
		list.add("");
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + attach.attachmentInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at Weapon level " + attach.attachmentInfo.weaponUnlockLevel);
			list.add("");
		}else if(!showGun){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + attach.attachmentInfo.name + ChatColor.BLUE + " - Attachment");
		}
		list.add(ChatColor.WHITE + attach.attachmentInfo.description);
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}
}
