package me.petersoj.weapons.guns;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.grenades.Lethal;
import me.petersoj.weapons.grenades.Tactical;
import me.petersoj.weapons.perk.Perk;

public class GunClass {
	
	public Gun primary;
	public Gun secondary;
	
	public Lethal lethal1;
	public Lethal lethal2;
	
	public Tactical tactical1;
	public Tactical tactical2;
	
	public Perk perk;
	
	public ItemStack classItemStack;
	
	public GunClass(){
		
	}

}
