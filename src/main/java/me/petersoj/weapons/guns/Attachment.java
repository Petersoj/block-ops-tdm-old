package me.petersoj.weapons.guns;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.guns.AttachmentManager.AttachmentInfo;

public class Attachment {
	
	public AttachmentInfo attachmentInfo;
	public ItemStack itemstack;
	
	public Attachment(AttachmentInfo attachmentInfo){
		this.attachmentInfo = attachmentInfo;
	}
}
