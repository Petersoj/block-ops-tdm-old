package me.petersoj.weapons.scorestreak;

import org.bukkit.inventory.ItemStack;

import me.petersoj.weapons.scorestreak.ScoreStreakManager.ScoreStreakInfo;

public class ScoreStreak {

	public ItemStack itemstack;
	public ScoreStreakInfo scoreStreakInfo;
	
	public ScoreStreak(ScoreStreakInfo info){
		this.scoreStreakInfo = info;
	}
}
