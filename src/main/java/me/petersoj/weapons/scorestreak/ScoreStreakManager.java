package me.petersoj.weapons.scorestreak;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.petersoj.data.data.PlayerData;
import me.petersoj.game.BlockOps;

public class ScoreStreakManager {
	
	public ScoreStreak none,pack,preditorMissile,shopper,advancedUAV;

	public ScoreStreakManager(){
		none=new ScoreStreak(ScoreStreakInfo.NONE);pack=new ScoreStreak(ScoreStreakInfo.PACK);
		preditorMissile=new ScoreStreak(ScoreStreakInfo.PREDITOR_MISSILE);
		shopper=new ScoreStreak(ScoreStreakInfo.SHOPPER);advancedUAV=new ScoreStreak(ScoreStreakInfo.ADVANCED_UAV);
		
		this.setScoreStreakItemStack(null, none, true, true);
		this.setScoreStreakItemStack(null, pack, true, true);
		this.setScoreStreakItemStack(null, preditorMissile, true, true);
		this.setScoreStreakItemStack(null, shopper, true, true);
		this.setScoreStreakItemStack(null, advancedUAV, true, true);
	}
	public enum ScoreStreakInfo{
		NONE("None", "", '!', '⣟', 0, 0),
		PACK("P.A.C.K", "Stun", 't', '⣟', 4, 4),
		PREDITOR_MISSILE("Preditor Missile", "Flash", 'u', '⣟', 4, 4),
		SHOPPER("Shopper", "Flash", 'v', '⣟', 4, 4),
		ADVANCED_UAV("Advanced UAV", "Flash", 'w', '⣟', 4, 4);
		
		public String name,description;
		public char character,visualCharacter;
		public int unlockLevel,durability;
		private ScoreStreakInfo(String name, String description, char character, char visual, int unlockLevel, int durability){
			this.name = name; this.description = description; this.character = character; this.visualCharacter = visual;
			this.unlockLevel = unlockLevel; this.durability = durability;
		}
	}
	
	public ScoreStreak[] getDefaultScoreStreakArray(){
		return new ScoreStreak[]{none,pack,preditorMissile,shopper,advancedUAV};
	}
	
	public void setScoreStreakItemStack(PlayerData pData, ScoreStreak streak, boolean show, boolean locked){
		new BukkitRunnable() {
			@Override
			public void run() {
				streak.itemstack = getScoreStreakItem(pData, streak, show, locked);
			}
		}.runTask(BlockOps.getInstance());
	}
	
	private ItemStack getScoreStreakItem(PlayerData pData, ScoreStreak streak, boolean showGun, boolean locked){
		if(streak == none){
			return BlockOps.getWeaponGUI().noneItem;
		}
		ItemStack item = new ItemStack(Material.DIAMOND_PICKAXE,1,(short)streak.scoreStreakInfo.durability);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + streak.scoreStreakInfo.name + ChatColor.BLUE + " - ScoreStreak");
		List<String> list = new ArrayList<String>();
		list.add("");
		if(locked){
			meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + streak.scoreStreakInfo.name + ChatColor.RED + "" + ChatColor.BOLD + " - LOCKED");
			list.add(ChatColor.BLUE + "Unlock at level " + streak.scoreStreakInfo.unlockLevel);
			list.add("");
		}else if(showGun){
			meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.UNDERLINE + "" + ChatColor.BOLD + streak.scoreStreakInfo.name
					+ ChatColor.WHITE + " - " + ChatColor.GOLD + "You have " + /*TODO PUT COIN EMOJI HERE*/String.valueOf(pData.coins) +" coins");
			list.add(ChatColor.GOLD + /*TODO PUT COIN EMOJI HERE*/"1 Coin");
			list.add("");
		}
		list.add(ChatColor.WHITE + streak.scoreStreakInfo.description);
		meta.spigot().setUnbreakable(true);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_UNBREAKABLE, ItemFlag.HIDE_ENCHANTS);
		meta.setLore(list);
		item.setItemMeta(meta);
		return item;
	}

}
