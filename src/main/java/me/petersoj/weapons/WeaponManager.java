package me.petersoj.weapons;

import me.petersoj.weapons.grenades.LethalManager;
import me.petersoj.weapons.grenades.TacticalManager;
import me.petersoj.weapons.guns.AttachmentManager;
import me.petersoj.weapons.guns.GunManager;
import me.petersoj.weapons.perk.PerkManager;
import me.petersoj.weapons.scorestreak.ScoreStreakManager;

public class WeaponManager {
	
	private LethalManager lethal;
	private TacticalManager tactical;
	private GunManager gun;
	private AttachmentManager attach;
	private PerkManager perk;
	private ScoreStreakManager score;
	
	public WeaponManager(){
		lethal = new LethalManager();
		tactical = new TacticalManager();
		gun = new GunManager();
		attach = new AttachmentManager();
		perk = new PerkManager();
		score = new ScoreStreakManager();
	}
	
	public LethalManager getLethalManager(){
		return lethal;
	}
	public TacticalManager getTacticalManager(){
		return tactical;
	}
	public GunManager getGunManager(){
		return gun;
	}
	public AttachmentManager getAttachmentManager(){
		return attach;
	}
	public PerkManager getPerkManager(){
		return perk;
	}
	public ScoreStreakManager getScoreStreakManager(){
		return score;
	}

}
